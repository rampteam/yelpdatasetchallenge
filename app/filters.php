<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	//
});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

/**
 * Set user info if is logged
 **/
Route::filter('yauth.setinfo', function(){
	if(!YAuth::getToken())
	{
		$token = Session::get('token', null);
		if($token)
			YAuth::token($token);
	}
});

/**
 * Allow only logger users 
 **/
Route::filter('yauth.app', function(){
	$token = Session::get('token', null);
	if($token === null)
		return Redirect::action('AuthController@getLogin')->withErrors(array('You must login to access this page'));

	$newToken = YAuth::token($token);
	if(!YAuth::check())
	{
		return Redirect::action('AuthController@getLogin')->withErrors(array('You must login to access this page'));
	}
});

/**
 *  Authentication for api using the token
 **/

Route::filter('auth.api', function(){

	$token = Request::header('X-Auth-Token', null);
	if($token === null)
		return Response::make('token invalid or not found', 404);

	$newToken = YAuth::token($token);
	if(!YAuth::check())
	{
		return Response::make('token invalid or not found', 404);
	}
});

/**
 * Deny acces to logger user
 **/
Route::filter('yauth.dennied', function(){
	if(YAuth::check())
	{
		App::abort(403, 'Access denied');
	}
});

Route::filter('auth', function()
{
	if (Auth::guest()) return Redirect::guest('login');
});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});


/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});

Route::filter('cache', 'Yelp\Filter\ApiCache@filter');


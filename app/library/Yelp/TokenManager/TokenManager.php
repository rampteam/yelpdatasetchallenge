<?php namespace Yelp\TokenManager;

use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;


/**
 * Class TokenManager
 * Used to store and retrieve auth informations
 * @package Yelp\TokenManager
 */
class TokenManager
{
    /**
     * @var cached data associated with a token
     */
    protected $tokenData = array();

    /**
     * create a token and store it
     *
     * @param array $data used for creating a token
     * @param array $aditionalData additional data
     *
     * @return string
     */
    public function createToken(array $data, array $aditionalData = array())
    {
        $aditionalData = array_merge($aditionalData, array('createTime' => time()));
        $tokenId = $this->generateId($data, $aditionalData);
        $value = $this->generateValue($data, $aditionalData);

        Session::put($tokenId, $value);
        return $tokenId;
    }

    /**
     * Set token data
     *
     * @param $id
     * @param array $data
     */
    protected function  setTokenData($id, array $data)
    {
        $this->tokenData[$id] = $data;
    }

    /**
     * Check if a token is valid
     *
     * @param $token
     * @return bool
     */
    public function isValid($token)
    {
        return $this->decrypt($token) !== false;
    }

    /**
     * get token data
     *
     * @param $token
     * @return bool
     */
    public function getData($token)
    {
        if (!$this->isValid($token))
        {
            return false;
        }
        $data = $this->decrypt($token);
        return $data['data'];
    }

    /**
     * get token aditional data
     *
     * @param $token
     * @return bool|array
     */
    public function getAditionalData($token)
    {
        if (!$this->isValid($token))
        {
            return false;
        }
        $data = $this->decrypt($token);
        return $data['aditional'];
    }

    /**
     * remove a token from session
     *
     * @param $token
     * @return bool
     */
    public function delete($token)
    {
        if (!$this->isValid($token))
        {
            return false;
        }
        Session::forget($token);
    }

    /**
     * get token informations
     * @param $id
     * @return bool|array
     */
    protected function decrypt($id)
    {
        if (!Session::has($id))
        {
            return false;
        }

        if (isset($this->tokenData[$id]))
        {
            return $this->tokenData[$id];
        }

        $tokenValue = Session::get($id);
        $data = trim(Crypt::decrypt($tokenValue));
        $data = json_decode($data, true);
        if (json_last_error() === JSON_ERROR_NONE)
        {
            return $data;
        }
        return false;
    }

    /**
     * Generate a signed token based on token data
     *
     * @param array $data
     * @param array $aditionalData
     * @return string
     */
    protected function generateId(array $data, array $aditionalData)
    {
        $strong = true;
        $rand =  openssl_random_pseudo_bytes(127, $strong);
        $token = hash('sha256', $rand);
        $token = json_encode($data) . $token . json_encode($aditionalData);
        return Crypt::encrypt($token);
    }

    /**
     * generate session encrypted data
     *
     * @param array $data
     * @param $aditionalData
     * @return string
     */
    protected function generateValue(array $data, $aditionalData)
    {
        $session = array('data' => $data, 'aditional' => $aditionalData);
        return Crypt::encrypt(json_encode($session));
    }
} 
<?php namespace Yelp\Auth\User;


use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseUser implements YUserInterface
{

    /**
     * To be auth credentials
     * @var array
     */
    protected $credentials = array();

    /**
     * @var user unique identifier
     */
    protected $userId;

    /**
     * User is authenticated flag
     *
     * @var bool
     */
    protected $authenticated = false;

    /**
     * aditional data to be persisted
     *
     * @var array
     */
    protected $aditionalInfo = array();

    public function __construct(array $credentials)
    {
        $this->credentials = $credentials;
    }

    /**
     * Authenticate a user
     *
     * @return boolean
     */
    public function authenticate()
    {
        $query = DB::table(Config::get('auth.table'));

        foreach ($this->credentials as $key => $value)
        {
            if (!str_contains($key, 'password'))
            {
                $query->where($key, $value);
            }
        }

        $user = $query->first();
        if ($user === null)
            return false;

        if (!isset($this->credentials['password']) || !is_string($this->credentials['password']))
            return false;

        if (Hash::check($this->credentials['password'], $user->password))
        {
            $this->userId = $user->id;
            $this->setPersistendData((array)$user);
            $this->authenticated = true;
            return true;
        }
        return false;
    }

    /**
     * Check if a user is authenticated
     *
     * @return boolean
     */
    public function check()
    {
        return $this->authenticated;
    }

    /**
     * returns a value that uniquely represents the user
     *
     * @return mixed
     */
    public function getIdentifier()
    {
        return $this->userId;
    }

    /**
     * return aditional information to be persisted in token
     *
     * @return array
     */
    public function getPersistentData()
    {
        return $this->aditionalInfo;
    }

    /**
     * set persistent info
     *
     * @param array $data
     */
    public function setPersistendData(array $data)
    {
        $this->aditionalInfo = $data;
    }

    /**
     * get a data piece
     *
     * @param $key
     * @param null $default
     * @return mixed
     */
    public function get($key, $default = null)
    {
        if (isset($this->aditionalInfo[$key]))
            return $this->aditionalInfo[$key];
        return $default;
    }

    /**
     * set a data piece
     *
     * @param $key
     * @param $value
     */
    public function set($key, $value)
    {
        $this->aditionalInfo[$key] = $value;
    }

    /**
     * remove a data piece
     *
     * @param $key
     */
    public function remove($key)
    {
        unset($this->aditionalInfo[$key]);
    }
}
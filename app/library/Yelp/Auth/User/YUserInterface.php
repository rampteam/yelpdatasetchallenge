<?php namespace Yelp\Auth\User;


interface YUserInterface {

    /**
     * Authenticate a user
     *
     * @return boolean
     */
    public function authenticate();

    /**
     * Check if a user is authenticated
     *
     * @return boolean
     */
    public function check();

    /**
     * returns a value that uniquely represents the user
     *
     * @return mixed
     */
    public function getIdentifier();

    /**
     * return aditional information to be persisted in token
     *
     * @return array
     */
    public function getPersistentData();
} 
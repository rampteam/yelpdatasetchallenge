<?php 

namespace Yelp\Processing;

use Yelp\MongoDB\Aggregation;
use Yelp\Support\Util;

class Base 
{	

	protected $mongoClient = null;
	
	function __construct()
	{
		

	}


	protected function getAggBuilder()
	{	
    	//get mongo client
		return new Aggregation($this->getMongoClient());
	}


	protected function getMongoClient()
	{
		if(!$this->mongoClient instanceof \MongoClient)
		{
			$mc = \Config::get('database.connections.mongodb');

			$this->mongoClient = new \MongoClient("mongodb://{$mc['host']}:{$mc['port']}");
		}

		return $this->mongoClient;

	}

	protected function getMongoDB($dbName)
	{
		$client = $this->getMongoClient();

		return $client->selectDB($dbName);
	}


	protected function getDataKeys(&$defaults = array(), $from)
	{
		return Util::getDataKeys($defaults, $from);
	}

	protected function getSetDataKeys($defaults = array(), $from)
	{	
		return Util::getSetDataKeys($defaults, $from);
	}



}
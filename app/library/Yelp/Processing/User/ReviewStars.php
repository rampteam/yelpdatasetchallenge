<?php 

namespace Yelp\Processing\User;
use Yelp\Model\YUser;
use Yelp\Processing\Base;
use Yelp\Model\YBusiness;
use Yelp\Model\YReview;

class ReviewStars extends Base
{
	protected $userId = null;
	protected $links = array();
	
	public function __construct($userId = array())
	{
		$this->userId = $userId;
	}

	public function getReviewStars()
	{
		$data = $this->getCityBusiness();

		if(empty($data)) return array();

		$responseData = array();

		foreach($data as $cityData) 
		{	

			$stats = $this->getReviewStats($cityData["businesses"]);
            if(empty($stats)) continue;
			$responseData[] = $this->getCityStateData($cityData, $stats);
			
		}

		return $responseData;
 	}

 	protected function getCityStateData($cityData, $stats)
 	{	
 		$inputData = array_merge($cityData, $stats);
 		
 		$data = array();
 		
 		$defaults = array(
 			"avg_stars" => 0,
 			"nr_reviews" => 0,
 			"user_id" => $this->userId,
 			"city" => "unknown",
 			"state" => "unknown"
 		);

 		$data = \YUtil::getDataKeys($defaults, $inputData);

        $data["avg_stars"] = round($data["avg_stars"], 2);

        return $data;
 	}

	protected function getCityBusiness()
	{		
		$agg = $this->getAggBuilder();

        $p = array();

        $groupId = $agg->getCollMap(array("state", "city")); 

        $groupBody = array(
            "businesses" => array('$addToSet' => '$business_id')
        );

        $agg->bindGroup($p, $groupId, $groupBody);

        $params = array_merge(YBusiness::getDBColl() ,array('pipeline' => $p));

        $data = $agg->exec(
            $params
        );

        $agg->flatIds($data);

        return $data;
	}

	protected function getReviewStats(array $businesses)
	{
		$agg = $this->getAggBuilder();

        $p = array();


        //match pipe first
        $filters = array(
        	"user_id" => array($this->userId),
        	"business_id" => $businesses
        );

        $params = array(
        	"filters" => $filters
        );

        $agg->bindMatch($p, $params);

        $groupId = $agg->getCollMap(array("user_id")); 

        $groupBody = array(
            "nr_reviews" => array('$sum' => 1),
            "avg_stars" => array('$avg' => '$stars')
        );

        $agg->bindGroup($p, $groupId, $groupBody);

        //remove cities with no review

        $params = array(
            "nr_reviews" => 
                    array(
                        '$gt' => 0
                    )
        );

        $agg->bindRawMatch($p, $params);

        $params = array_merge(YReview::getDBColl() ,array('pipeline' => $p));

        $data = $agg->exec(
            $params
        );	

        $agg->flatIds($data);

        //dump($data);
        if(is_array($data) && count($data) == 1)
        {
        	return array_shift($data);
        }


        return $data;
	}


}
<?php 

namespace Yelp\Processing\User;
use Yelp\Processing\Base;
use Yelp\Model\YReview;
use Yelp\Model\YTip;
use Yelp\Model\YBusiness;

class Businesses extends Base
{
	protected $userId = null;
	protected $links = array();
	
	public function __construct($userId = array())
	{
		$this->userId = $userId;
	}

	public function getUserBusinessData()
	{
        $userReviews = $this->getUserReviewsData();
		$userTips = $this->getUserTipsData();

        $userData = array();

        if(empty($userReviews) && empty($userTips))
        {
            return $userData;
        }

        $userTipsAssoc = \YUtil::makeAssocByKey($userTips, "business_id");

        //get all business ids and load coord
        $reviewBIds = array_pluck($userReviews, 'business_id');

        $businessIds = array_merge($reviewBIds, array_keys($userTipsAssoc));

        $businessCoord = $this->getBusinessCoord($businessIds);
    
        foreach ($userReviews as $review) 
        {
            $tipInfo = $this->getTipInfo($userTipsAssoc, $review["business_id"]);
            
            $toAppend = array_merge($review, $tipInfo);
            if(isset($businessCoord[$review['business_id']]))
            {
                $toAppend = array_merge($toAppend, $businessCoord[$review['business_id']]);
            }
            
            $userData[] = $toAppend;

        }
        
        return $userData;
	}

    private function getTipInfo($tips, $businessId)
    {
        $tipInfo = array(
            "nr_tips" => 0,
            "avg_likes" => 0
        );  

        if(isset($tips[$businessId]))
        {
            $tipInfo = array_merge($tipInfo, $tips[$businessId]);
        }

        return $tipInfo;
    }

    protected function getBusinessCoord(array $businessIds)
    {       
        $bussinesses = array();

        if(empty($businessIds))
        {
            return $bussinesses;
        }


        $query = YBusiness::query();
        
        // apply filter options
        YBusiness::applyFilter($query, array("business_id" => $businessIds));

        $bussinesses = $query->get(array("business_id", "coord", "name"));

        if(empty($bussinesses)) 
        {
            return array();
        }

        $businessesAssoc = \YUtil::makeAssocByKey($bussinesses, 'business_id');

        if(empty($businessesAssoc))
        {
            return array();
        }

        return $businessesAssoc;
    }

	public function getUserReviewsData()
	{
		$agg = $this->getAggBuilder();

        $p = array();

        //match pipe first
        $filters = array(
        	"user_id" => array($this->userId)
        );

        $params = array(
        	"filters" => $filters
        );

        $agg->bindMatch($p, $params);

        $groupId = $agg->getCollMap(array("user_id", "business_id")); 

        $groupBody = array(
            "nr_reviews" => array('$sum' => 1),
            "avg_stars" => array('$avg' => '$stars')
        );

        $agg->bindGroup($p, $groupId, $groupBody);

        $params = array_merge(YReview::getDBColl() ,array('pipeline' => $p));

        $data = $agg->exec(
            $params
        );	

        $agg->flatIds($data);

        return $data;
	}

    public function getUserTipsData()
    {
        $agg = $this->getAggBuilder();

        $p = array();

        //match pipe first
        $filters = array(
            "user_id" => array($this->userId)
        );

        $params = array(
            "filters" => $filters
        );

        $agg->bindMatch($p, $params);

        $groupId = $agg->getCollMap(array("user_id", "business_id")); 

        $groupBody = array(
            "nr_tips" => array('$sum' => 1),
            "avg_likes" => array('$avg' => '$likes')
        );

        $agg->bindGroup($p, $groupId, $groupBody);

        $params = array_merge(YTip::getDBColl() ,array('pipeline' => $p));

        $data = $agg->exec(
            $params
        );  

        $agg->flatIds($data);

        return $data;
    }
}
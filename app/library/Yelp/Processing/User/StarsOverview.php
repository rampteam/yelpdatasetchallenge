<?php 

namespace Yelp\Processing\User;
use Yelp\Model\YUser;
use Yelp\Processing\Base;
use Yelp\Model\YBusiness;
use Yelp\Model\YReview;

class StarsOverview extends Base
{
	public function getStarsOverview($userId)
	{
		$agg = $this->getAggBuilder();

        $p = array();

        $filters = array(
            "user_id" => array($userId),
        );

        $params = array(
            "filters" => $filters
        );

        $agg->bindMatch($p, $params);

        $groupId = $agg->getCollMap(array("user_id", "stars")); 

        $groupBody = array(
            "reviews" => array('$sum' => 1)
        );

        $agg->bindGroup($p, $groupId, $groupBody);

        $params = array_merge(YReview::getDBColl() ,array('pipeline' => $p));

        $data = $agg->exec(
            $params
        );

        $agg->flatIds($data);

        
        return $this->getResponse($data);
 	}

    protected function getResponse($data)
    {   
        $newData = array();
        foreach ($data as $nr => $item) 
        {   
            unset($item["user_id"]);
            $newData[strval($item["stars"])] = $item;
        }

        $data = array();
        foreach (range(1, 5, 0.5) as $number) 
        {
            $nr = strval($number);

            if(!isset($newData[$nr]))
            {
                $newData[$nr] = array(
                    "reviews" => 0,
                    "stars" => $nr
                );
            }
        }


        return array_values($newData);
    }

}
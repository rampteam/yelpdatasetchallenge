<?php 

namespace Yelp\Processing\User;
use Yelp\Model\YUser;
use Yelp\Model\YBusiness;
use Yelp\Model\YReview;
use Yelp\Processing\Base;

class FriendsGraph extends Base
{
	protected $requestData = array();

	protected $links = array();
	
	public function __construct($requestData = array())
	{
		$this->requestData = $requestData;
	}

	protected function getUsersFromBusinesses()
	{	
		$usersIds = array();

		$options = $this->requestData;

		$businessFilters = YBusiness::getValidFiltersOnly($options['filter']);

		if(empty($businessFilters)) return $usersIds;
		
		$query = YBusiness::query();

        YBusiness::applyFilter($query, $businessFilters);
       	
        $businesses = array_pluck($query->get(array("business_id")), 'business_id');

        if(!is_array($businesses))
        {
        	return $usersIds;
        }

        $businesses = array_unique($businesses);

        if(empty($businesses))
        {
        	return $usersIds;
        }

        //use an aggregation pipe to extract from reviews all users

     	$usersIds = $this->getUsersFromReviews($businesses);

        return array_unique($usersIds);
	}


	protected function getUsersFromReviews(array $businessIds)
	{

		$agg = $this->getAggBuilder();

        $p = array();

        //match pipe first
        $filters = array(
        	"business_id" => $businessIds
        );

        $params = array(
        	"filters" => $filters
        );

        $agg->bindMatch($p, $params);

        $groupId = array(); 

        $groupBody = array(
            "users" => array('$addToSet' => '$user_id')
        );

        $agg->bindGroup($p, $groupId, $groupBody);

        $params = array_merge(YReview::getDBColl() ,array('pipeline' => $p));

        $data = $agg->exec(
            $params
        );	

        if(is_array($data))
        {	
        	$row = array_shift($data);
        	if(!empty($row["users"]))
        	{
        		return $row["users"];
        	}
        	
        }else
        {
        	 return array();
        }
      
	}

	protected function businessHasFilters()
	{
		return YBusiness::haveValidFilter($this->requestData['filter']);
	}

	protected function getRawUsers()
	{		
		$userIds = $this->getUsersFromBusinesses();

		//check if we actually have valid filters for business 
		if($this->businessHasFilters() && empty($userIds))
		{
			return $userIds;
		}
	
		if($this->isNrFriends())
		{
			return $this->getUsersWithNrFriends($userIds);
		}else
		{
			return $this->getUsersBasic($userIds);
		}
	}

	protected function getUsersWithNrFriends($userIds = array())
	{	
		$usersData = array();

		$query = YUser::yelpQuery();

		$options = $this->requestData;

		$this->applyQueryFilters($query, $userIds);

		$rawMatch = $query->getRawWhere();

		$agg = $this->getAggBuilder();

        $p = array();

        if(!empty($rawMatch))
        {
        	$agg->bindRawMatch($p, $rawMatch);
        }
        

        $allColumns = YUser::getAllColumns();

        $agg->bindUnwind($p, '$friends');

        $groupId = $agg->getCollMap(array("user_id")); 

        $groupBody = array(
            "nr_friends" => array('$sum' => 1),
        );

        $fields = $agg->getCollMap($allColumns);
        //$groupBody = array_merge($groupBody, $fields);

        $agg->bindGroup($p, $groupId, $groupBody);

        if(isset($options['filter']['nr_friends']))
        {
        	$nrFriendsQuery = YUser::yelpQuery();
        	YUser::applyFilterBasedOnValue($nrFriendsQuery, 'nr_friends', $options['filter']['nr_friends']);

        	$rawMatch = $nrFriendsQuery->getRawWhere();

        	 if(!empty($rawMatch))
        	{
        		$agg->bindRawMatch($p, $rawMatch);
        	}

        }

        //$options["orderBy"] = array('nr_friends' => 'asc');
        $orderBy = YUser::getValidColumnData($options["orderBy"], array('nr_friends'));

        $agg->bindSort($p,  $orderBy);


          //apply offset
        $agg->bindOffset($p, $options["offset"]);

        //apply limit
        //
        $agg->bindLimit($p, $options["limit"]);


        //dump($p);
        $params = array_merge(YUser::getDBColl() ,array('pipeline' => $p));

        $data = $agg->exec(
            $params
        );	

        $agg->flatIds($data);

        if(!empty($data))
        {	
        	$userIds = array_pluck($data, "user_id");

        	$query = YUser::query();

        	$this->applyFilterByUserId($query, $userIds);

        	$users = $query->get();

        	if(!is_array($users)) return array();

        	$assocUsers = $this->extractIds($users);

        	//to preserver order by 
        	foreach ($userIds as $userId) 
        	{	
        		$usersData[] = array_get($assocUsers, $userId, array());
        	}

        	return $usersData;
        }
	}

	protected function getUsersBasic($userIds = array())
	{
		$query = YUser::query();

		$options = $this->requestData;

		$this->applyQueryFilters($query, $userIds);

		// apply limit and offset
        YUser::applyLimit($query, $options['limit']);
        YUser::applyOffset($query, $options['offset']);
       	
       	// set order by
        YUser::applyOrder($query, $options['orderBy']);

        $users = $query->get();
     
        return $users;
	}

	protected function applyQueryFilters($query, $userIds = array())
	{
		$options = $this->requestData;

        $userFilters = YUser::getValidFiltersOnly($options['filter']);

        YUser::applyFilter($query, $userFilters);

        $this->applyFilterByUserId($query, $userIds);
       
        YUser::applySearch($query, $options['search'], array('name'));
	}

	protected function applyFilterByUserId($query, $userIds)
	{	

		if(!empty($userIds))
        {
        	YUser::applyFilter($query, array("user_id" =>  $userIds));
        }
	}

	private function isNrFriends()
	{	
		return (isset($options['filter']['nr_friends']) || isset($options['orderBy']['nr_friends']));
	}

	public function getUsers()
	{
		$users = $this->getRawUsers();

		if(is_array($users) && empty($users))
		{
			return $users;
		}

		$assocUsers = $this->extractIds($users);

		$existingUsers = array_keys($assocUsers);

		$columns = is_array($this->requestData['columns']) ? $this->requestData['columns'] : null;
		if($columns)
		{
			array_push($columns, "links", "index");
		}		
		//update 
		foreach ($assocUsers as $userId => &$userData) 
		{	
			$userData["links"] = array();

			//parse friends
			foreach ($userData["friends"] as $fId) 
			{
				//if friends exists in this set
				if(in_array($fId, $existingUsers))
				{	
					$fIndex = $assocUsers[$fId]["index"];
					$userData["links"][] = $fIndex;

					$this->setLink($userData["index"], $fIndex);
				}
			}

			if($columns)
			{
				$userData = array_only($userData, $columns);	
			}
			
		}

		return array(
			"users" => array_values($assocUsers),
			"links" => array_values($this->links)
		);
	}

	protected function setLink($source, $target)
	{	
		$st = $source."_".$target;
		$ts = $target."_".$source;
		if(array_key_exists($st, $this->links)) return ;
		if(array_key_exists($ts, $this->links)) return ;

		$this->links[$st]  = array(
			"source" => ($source),
			"target" => ($target)
		);
	}

	protected function extractIds($users)
	{	
		$assocUsers = array();
		foreach ($users as $nI => $user) 
		{	
			$userId = array_get($user, 'user_id', null);
			if(!$userId) continue;

			//next bind an id
			$user["index"] = $nI; 

			$assocUsers[$userId] = $user;
		}

		return $assocUsers;
	}

}
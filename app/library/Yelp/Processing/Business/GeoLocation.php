<?php 

namespace Yelp\Processing\Business;
use Yelp\Model\YBusiness;

use  Yelp\Processing\Base;

class GeoLocation extends Base
{
	
	protected $inputOptions = array();

	public function __construct($inputOptions = array())
	{
		$this->inputOptions = $inputOptions;
	}

	public function getData()
	{
		$type = array_get($this->inputOptions, 'type', 'near');

		switch ($type) 
		{
			case 'exact':
				return $this->getExactData();
				break;
			case 'geoNear':
				return $this->getGeoNearData();
				break;
			
			case 'near':
			default:
			return $this->getNearData();
			break;
		}
	}

	protected function getBaseQuery()
	{
		// create query
        $query = YBusiness::query();
        
        // apply filter options
        YBusiness::applyFilter($query, $this->inputOptions['filter']);

        return $query;
        
	}

	public function getNearData()
	{
		$query = $this->getBaseQuery();

		$mx = array("maxDistance" 	=> null);
		$data = $this->getDataKeys($mx, $this->inputOptions);
		extract($data);

		$cmd = array('$near' => $this->getCoord());
		if($maxDistance !== null)
		{
			$cmd['$maxDistance'] = $maxDistance;
		}

		$query->whereRaw(array('coord' => $cmd ));

		YBusiness::applyLimit($query, $this->inputOptions["limit"]);
		
		return $this->getWithColumns($query);
	}

	public function getExactData()
	{
		$query = $this->getBaseQuery();

		return $this->getWithColumns();
	}


 	public function getGeoNearData()
	{
		$db = $this->getMongoDB(YBusiness::getDBName());

		$command = array(
			'geoNear' => YBusiness::getCollName(),
			'near' => $this->getCoord(),
			'query' => $this->getGeoQuery(),
			"distanceMultiplier" => ( (6371 * pi() / 180.0))
		);

		$params = array("maxDistance", "limit", "distanceMultiplier");

		$data = $this->getSetDataKeys($params, $this->inputOptions);

		//convert limit to integer
		if(isset($data["limit"]))
		{
			$data["limit"] = intval($data["limit"]);
		}


		foreach ($data as $key => $value) 
		{
			$command[$key] = $value;	
		}

		$result = $db->command($command);

		if(empty($result["ok"]))
		{
			throw new \Exception(YUtil::getMongoExMsg("Geolocation query failed!"));
		}

		$results =  array_get($result, "results", array());

		if(!empty($results))
		{
			foreach ($results as &$business) 
			{
				$business["obj"] = \YUtil::extractColumns(array($business["obj"]), $this->inputOptions["columns"]);
			}
		}

		return $results;

	}

	protected function getGeoQuery()
	{	
		$query = array();

		$filters = $this->inputOptions["filter"];
		if(!empty($filters))
		{	
			foreach ($filterVals as $coll => $value) 
			{
				if(is_array($value))
				{	
					$query[$coll]  = array('$in' => $value);
				}else
				{
					$query[$coll]  = $value;
				}
			}
		}

		return $query;
	}

	protected function getCoord()
	{
		$paramList = array(
			"longitude" 	=> null, 
			"latitude" 		=> null, 
		);

		$data = $this->getDataKeys($paramList, $this->inputOptions);

		extract($data);

		if(!isset($longitude, $latitude))
		{
			throw new \InvalidArgumentException("Could not extract longitude and latitude!");
		}

		return array_map("floatval", array_values($data));
	}

	protected function getColls()
	{
		return empty($this->inputOptions["columns"])? null: $this->inputOptions["columns"];
	}

	protected function getWithColumns($query)
	{
		if($this->getColls())
		{
			return $query->get($this->getColls());
		}else
		{
			return $query->get();
		}
	}

}
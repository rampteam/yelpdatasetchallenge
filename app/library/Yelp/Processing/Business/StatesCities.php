<?php 


namespace Yelp\Processing\Business;
use Yelp\Model\YBusiness;
use Yelp\Processing\Base;

class StatesCities extends Base
{
	protected $inputOptions = array();
	
	public function __construct($inputOptions = array())
	{
		$this->inputOptions = $inputOptions;
	}

	public function getStatesAndCities($inputOptions = array())
    {
        $agg = $this->getAggBuilder();

        $p = array();
        //$agg->bindMatch($p, $params);

        $groupId = $agg->getCollMap(array("state")); 

        $groupBody = array(
            "cities" => array('$addToSet' => '$city')
        );

        $agg->bindGroup($p, $groupId, $groupBody);

        $params = array_merge(YBusiness::getDBColl() ,array('pipeline' => $p));

        $data = $agg->exec(
            $params
        );

        $agg->flatIds($data);
        

        $states = array();
        foreach ($data as $stateData) 
        {
            $state = array();
            $state["short_name"] = $state["long_name"]  = ucwords($stateData["state"]);
            $state["cities"] = array();

            foreach ($stateData["cities"] as $cityName) 
            {
                $state["cities"][] = array("name" => $cityName);
            }

            $states[] = $state;
        }

       return $states;
        
    }


    protected function getStateAbbreviations()
    {

      return array(
         "Alabama"       => "AL",
         "Alaska"        => "AK",
         "Arizona"       => "AZ",
         "Arkansas"      => "AR",
         "California"    => "CA",
         "Colorado"      => "CO",
         "Connecticut"   => "CT",
         "Delaware"      => "DE",
         "Florida"       => "FL",
         "Georgia"       => "GA",
         "Hawaii"        => "HI",
         "Idaho"         => "ID",
         "Illinois"      => "IL",
         "Indiana"       => "IN",
         "Iowa"          => "IA",
         "Kansas"        => "KS",
         "Kentucky"      => "KY",
         "Louisiana"     => "LA",
         "Maine"         => "ME",
         "Maryland"      => "MD",
         "Massachusetts" => "MA",
         "Michigan"      => "MI",
         "Minnesota"     => "MN",
         "Mississippi"   => "MS",
         "Missouri"      => "MO",
         "Montana"       => "MT",
         "Nebraska"      => "NE",
         "Nevada"        => "NV",
         "New Hampshire" => "NH",
         "New Jersey"    => "NJ",
         "New Mexico"    => "NM",
         "New York"      => "NY",
         "North Carolina"=> "NC",
         "North Dakota"      => "ND",
         "Ohio"      => "OH",
         "Oklahoma"      => "OK",
         "Oregon"        => "OR",
         "Pennsylvania"      => "PA",
         "Rhode Island"      => "RI",
         "South Carolina"        => "SC",
         "South Dakota"      => "SD",
         "Tennessee"     => "TN",
         "Texas"     => "TX",
         "Utah"      => "UT",
         "Vermont"       => "VT",
         "Virginia"      => "VA",
         "Washington"        => "WA",
         "West Virginia"     => "WV",
         "Wisconsin"     => "WI",
         "Wyoming"       => "WY"
         );

    }

    protected function getAbbStates()
    {
        return array_flip($this->getStateAbbreviations());
    }
}
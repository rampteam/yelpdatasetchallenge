<?php 

namespace Yelp\Support;

class Util
{
	
	
	public static function extractColumns(array $data, $columns)
	{	
		$columns = (!empty($columns) && is_array($columns)) ? $columns : null;
			
		if(!$columns) return $data;

		$truncData = array();

		foreach ($data as $dataRow) 
		{		
			$singleRow = array();
			foreach ($columns as $col ) 
			{
				$singleRow[$col] = array_get($dataRow, $col, null);
			}

			$truncData[]  = $singleRow;	
		}

		return $truncData;
	}

	public static function getDataKeys(&$defaults = array(), $from)
	{
		foreach ($defaults as $key => &$data) 
		{
			$data = array_get($from, $key, $data);
		}

		return $defaults;
	}

	public static function getSetDataKeys($defaults = array(), $from)
	{	
		$data = array();

		foreach ($defaults as $key) 
		{
			$val = array_get($from, $key, null);
			if($val !== null)
			{
				$data[$key] = $val;
			}
		}

		return $data;
	}


	public static function getOrSetDataKeys($defaults = array(), $from)
	{	
		$data = array();

		foreach ($defaults as $key) 
		{
			$val = array_get($from, $key, null);
			$data[$key] = $val;
		}

		return $data;
	}

	public static function getMongoExMsg($msg = "" , array $result)
	{
		return "{$msg}: Msg - {$results['errmsg']} Code - {$results['code']}";
	}


	public static function isAssoc($arr)
	{
    	return array_keys($arr) !== range(0, count($arr) - 1);
	}

	public static function makeAssocByKey($rows, $key)
	{	
		$assocUsers = array();
		foreach ($rows as $nI => $row) 
		{	
			$dataKey = array_get($row, $key, null);
			if(!$dataKey) continue;

			$assocUsers[$dataKey] = $row;
		}

		return $assocUsers;
	}




}
<?php namespace Yelp\Validators;

/**
 * Edit user validators
 **/
class Edit extends Validator
{

	public static $rules = array(
		'firstName' => array( 'required','alpha'),
		'lastName' => array('required', 'alpha'),
		'password' => array('required_with:newpassword1'),
		'newpassword1' => array('required_with:password, newpassword2','min:6'),
		'newpassword2' => array('required_with:newpassword1','min:6', 'same:newpassword1')
		);
}

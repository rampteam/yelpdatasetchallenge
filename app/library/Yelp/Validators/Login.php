<?php namespace Yelp\Validators;

/**
* Login fields validator
*/
class Login extends Validator
{
	public static $rules = array(
		'email' => 'required',
		'password'  => 'required',
	);
}
<?php 

namespace Yelp\Mongodb\Eloquent;
use Jenssegers\Mongodb\Eloquent\Builder as EloquentBuilder;

class Builder extends EloquentBuilder
{
	
	 /**
     * The methods that should be returned from query builder.
     *
     * @var array
     */
    protected $passthru = array(
        'toSql', 'lists', 'insert', 'insertGetId', 'pluck',
        'count', 'min', 'max', 'avg', 'sum', 'exists', 'push', 'pull', 'getRawWhere'
    );
}
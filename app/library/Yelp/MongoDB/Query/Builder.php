<?php 

namespace Yelp\Mongodb\Query;
use \Jenssegers\Mongodb\Query\Builder as QueryBuilder;

class Builder extends QueryBuilder
{
	  /**
     * Execute the query as a fresh "select" statement.
     *
     * @param  array  $columns
     * @return array|static[]
     */
    public function getRawWhere()
    {
        // Compile wheres
        $wheres = $this->compileWheres();

        return $wheres;
    }
}
<?php 

namespace Yelp\MongoDB\DataInsertion;

use Yelp\MongoDB\DataInsertion\Collection\Composite;
use Exception;

class Base
{
	protected $config = array();

	protected $mongoClient = null;

	protected $collections = null;

	protected $statusData = array();

	function __construct(array $config = array())
	{
		$this->config = $config;	
	}

	protected function getMongoClient()
	{
		if(!$this->mongoClient instanceof \MongoClient)
		{
			$mc = $this->config;

			$this->mongoClient = new \MongoClient("mongodb://{$this->config['host']}:{$this->config['port']}");
		}

		return $this->mongoClient;

	}


	protected function getComposite()
	{
		if($this->collections === null)
		{
			$this->collections = new Composite($this->config["database"], $this->getMongoClient());
			$this->collections->loadSupported();
		}

		return $this->collections;
	}


	protected function msg($msg, $type)
	{
		$this->statusData[] = array("type" => $type, "msg" => $msg); 
	}

	protected function returnResponse($status)
	{
		return array(
			"ok" => $status,
			"status" => $this->statusData
		);
	}

}
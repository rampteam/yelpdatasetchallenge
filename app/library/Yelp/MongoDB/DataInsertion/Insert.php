<?php 

namespace Yelp\MongoDB\DataInsertion;

use Yelp\MongoDB\DataInsertion\Collection\Composite;
use Exception;

class Insert extends Base
{

	protected $file = null;

	function __construct($file, array $config = array())
	{	
		parent::__construct($config);
		$this->file = $file;	
	}

	public function insertData()
	{	
		try{
			$comp = $this->getComposite();

			$insert = $this->insert();

			$this->msg("Operation succesfull!", "info");

			return $this->returnResponse(true);

		} catch (Exception $e) {
			$this->msg($e->getMessage(), "error");
			return $this->returnResponse(false);
		}
	}

	protected function insert()
	{	
		$this->msg("Connect to mongo...!", "info");
		$client = $this->getMongoClient();

		$this->msg("Connect to mongo succesfully...!", "info");
		$f = fopen($this->file, "r");

        if($f === false){
            throw new \InvalidArgumentException("Cannot open file : {$this->file}!");
        }
	
        $nrLines = 0;
        $composite = $this->getComposite();

        while (!feof($f)) {
            $line = fgets($f);
	
            $row = json_decode($line, true);
            if(json_last_error() !== JSON_ERROR_NONE)  {
               //$this->info("Invalid JSON line encountered!");
                continue;
            }
            //Trailing blank lines
            if(is_array($row))
            {
	
            	$type = empty($row["type"]) ? null : $row["type"];
            	if($type === null)
            	{
            		//$this->info("Unable to determine row type!");
            		continue;
            	}
	
            	$coll = $client->selectCollection($this->config['database'], $type);
	
            	$composite->getCollection($type)->rowHook($row);
	
                $coll->insert($row);
	
                $nrLines++;
            }
	
        }
	
        fclose($f);

        $this->msg("Inserted succesfully {$nrLines} lines!", "info");

        return true;

	}


}
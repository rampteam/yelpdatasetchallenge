<?php 

namespace Yelp\MongoDB\DataInsertion\Collection;

class Tip extends Base 
{
	public function rowHook(&$row)
	{
		$this->setMongoDate($row, "date");
	}

	public function preIndexes()
	{
		$this->getMongoClient()->selectDB("admin")->command(array("setParameter"  => 1, "textSearchEnabled" => true));
	}

	public function createIndexes()
	{
		
		$this->preIndexes();
		$indexes = array(
			array("text" => "text"),
			array("business_id" => 1),
			array("user_id" => 1),
			array("date" => 1),
			array("likes" => 1),
		);

		$this->basicIndex('tip', $indexes);

	}

}
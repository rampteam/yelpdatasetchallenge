<?php 

namespace Yelp\MongoDB\DataInsertion\Collection;

class User extends Base 
{
	public function rowHook(&$row)
	{
		$this->setMongoDate($row, "yelping_since");
	}

	public function createIndexes()
	{
			
		$indexes = array(
			array("user_id" => 1),
			array("name" => 1),
			array("yelping_since" => 1),
			array("average_stars" => 1),
			array("review_count" => 1)
		);

		$this->basicIndex('user', $indexes);

	}

}
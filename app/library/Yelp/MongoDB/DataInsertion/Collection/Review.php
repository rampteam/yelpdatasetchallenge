<?php 

namespace Yelp\MongoDB\DataInsertion\Collection;

class Review extends Base 
{
	public function rowHook(&$row)
	{
		$this->setMongoDate($row, "date");
	}

	public function preIndexes()
	{
		$this->getMongoClient()->selectDB("admin")->command(array("setParameter"  => 1, "textSearchEnabled" => true));
	}

	public function createIndexes()
	{
		$this->preIndexes();
		$indexes = array(
			array("business_id" => 1),
			array("user_id" => 1),
			array("text" => "text"),
			array("stars" => 1),
			array("date" => 1)
		);

		$this->basicIndex('review', $indexes);

	}

}
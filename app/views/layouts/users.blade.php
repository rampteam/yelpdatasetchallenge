<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <title>
            @section('title')
                Yelp Dataset Challenge 
            @show   
        </title>

        @section('head-rel')
            <link rel="stylesheet" href="/static/css/master.min.css" />
        @show

        @section('head-scripts')
            <script src="/static/js/library/modernizr.js"></script>
        @show
    </head>
<body>
    <div class="row">
      <div class="large-12 columns fixed contain-to-grid">
        @section('page-header')
            <nav class="top-bar" data-topbar>
             	<ul class="title-area">
             	  <li class="name">
             	    <h1><a href="/">Yelp Dataset Challenge</a></h1>
             	  </li>
             	  <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
             	</ul>
	
            	<section class="top-bar-section">
            	    <!-- Right Nav Section -->
            	    <ul class="right">
            	    	@if (YAuth::check())
            	    		<li><a href="{{ URL::action('AuthController@getLogin') }}">Login</a></li>
                            <li class="active"><a href="{{ URL::action('AuthController@getLogout') }}">Logout</a></li>
            	    		<li class=""><a href="{{ URL::action('UsersController@edit', [YAuth::user()['id']]) }}">Edit Profile</a></li>
            	    	@else
            	    		<li><a href="{{ URL::action('UsersController@create' ) }}">Register</a></li>
            	    		<li class="active"><a href="{{ URL::action('AuthController@getLogin') }}">Login</a></li>
            	    	@endif

            	    </ul>
            	</section>
            </nav>
        @show
      </div>
    </div>
    <div class="row">
        <div class="large-12 columns">
        	<div class="row">
        		@section('errors')
        			@include ('_partials.errors')
        		@stop
        		
        	</div>
            <div class="row">
                    @section('content')
                      
                    @show
            </div>
        </div>
    </div>
    <div class="row">
        <div class="large-12 columns">
            @section('page-footer')
                <h2>Footer Twelve Columns</h2>
            @show
        </div>
    </div>
    @section('foot-scripts')
        <script src="/static/js/library/jquery.js"></script>
        <script src="/static/js/library/foundation.min.js"></script>
        <script>
            $(document).foundation();
        </script>
    @show
</body>
</html>
<div class="yreview-content">
	<div class="row text-center">
	    <div class="large-6 columns">
	   		<h6>
	   			Review for
	   			<br/>
	   			<a href="#ybusinesses/<%=businessId%>"><%=businessName%></a>
	   		</h6>
	   	</div>
	   	<div class="large-6 columns yreview-date">
	   		<h6><%=date%></h6>
	   	</div>
	</div>
	<div class="row">
		<div class="large-6 columns">
			<div class="text-center">
				<span>
	        		<% for (var i = 1; i <= 5; i++) { %>
	        			<% if(i <= stars ) { %>
	        				<span class="star-icon full">☆</span>
	        			<% } else if ( i - 0.5 === stars ) { %>
	        				<span class="star-icon half">☆</span>
	        			<% } else { %>
	        				<span class="star-icon">☆</span>
	        			<% } %>
					<% } %>
				</span>
			</div>
		</div>
		<div class="large-6 columns text-center">
			<h6>
				<a href="#yusers/<%=userId%>">
					<%=userName%>
				</a>
			</h6>
		</div>
	</div>
	<div class="row">
		<p class="text-center yreview-text"><%=text%></p>
	</div>
	<div class="row review-votes">
		<div class="large-10 columns large-centered yreview-button-group">
			<div class="row">
				<div class="button-bar centered">
  					<ul class="button-group radius round">
  					  <li><a href="javascript:void(0)" class="tiny button secondary"><%=votes.funny%> funny</a></li>
  					  <li><a href="javascript:void(0)" class="tiny button secondary"><%=votes.cool%> cool</a></li>
  					  <li><a href="javascript:void(0)" class="tiny button secondary"><%=votes.useful%> useful</a></li>
  					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
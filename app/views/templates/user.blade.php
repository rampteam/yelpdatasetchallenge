<div class="row user-profile">
	<div class="large-12 columns">
		<div class="row user-name small-centered text-center padding-top-bottom-2">
			<h2>
				<%=name%>
			</h2>
		</div>
		<div class="row text-center">
			<div class="large-6 small-centered columns padding-top-bottom-2 button disabled">
				<h6><%=fans%></h6>	
			</div>
		</div>
		<div class="row text-center">
			<div class="large-6  small-centered columns padding-top-bottom-2 button disabled">
				<h6>
					Average stars: <span class="active"><%=avgStars%></span> 
				</h6>
			</div>
		</div>
		
		<div class="row text-center">
			<a href="javascript:void(0)" id="userFriendsButton">
				<div class="large-6 small-centered columns padding-top-bottom-2 button">
					<%=friends%>
				</div>
			</a>
		</div>
		<div class="row">
			<div id="userFriendsModal" class="hide">
					<div id="modalContent"></div>
			</div>
		</div>
		<div class="row text-center">
			<a href="javascript:void(0)" id="userComplimentsButton">
				<div class="large-6 small-centered columns padding-top-bottom-2 button">
					<%=compliments%>
				</div>
			</a>
		</div>
		<div class="row">	
			<div id="userComplimentsModal">
				<div id="modalContent">
				</div>
			</div>
		</div>
		<div class="row small-centered text-center">
			<a href="javascript:void(0)" id="userReviewsButton">
				<div class="large-6 small-centered columns border padding-top-bottom-2 button">
						<%=reviews%>
				</div>
			</a>
		</div>
		<% if(eliteYears) { %>
			<div class="row text-center">
				<div class="large-6 small-centered columns padding-top-bottom-2 button disabled">
					Elite years: <%=eliteYears%>
				</div>
			</div>
		<% } %>
		<% if(yelpSince) { %>
			<div class="row padding-top-bottom-2">
				User since: <%=yelpSince%>
			</div>
		<% } %>
		<div class="row text-center">
			<a href="javascript:void(0)" "mapRendered"="0" id="userMapButton">
				<div class="large-6 small-centered columns padding-top-bottom-2 button">
					Places
				</div>
			</a>
		</div>
		<div id="userMapModal"></div>

		<div class="row text-center">
			<a href="javascript:void(0)" id="userStatisticsButton">
				<div class="large-6 small-centered columns padding-top-bottom-2 button">
					Statisticts
				</div>
			</a>
		</div>
		<div id="userStatisticsModal">
			<div id="modalContent">
				<div id="reviewTotalNumberChart" class="row"></div>
				<div id="reviewCityChart" class="row"></div>
			</div>
		</div>				
	</div>
</div>
<div class="b-map-tooltip">
	<h1 class="title"><a class="b-name" href="javascript:void(0)" data-businessId="<%=business_id%>">
					<%=name%></a> 
	</h1>
                	<div class="stars">
                		<% for (var i = 1; i <= 5; i++) { %>
                			<% if(i <= stars ) { %>
                				<span class="star-icon full">☆</span>
                			<% } else if ( i - 0.5 === stars ) { %>
                				<span class="star-icon half">☆</span>
                			<% } else { %>
                				<span class="star-icon">☆</span>
                			<% } %>
						<% } %>
                	</div>
                        <div class="review-count"><%=review_count%> Reviews</div>
                        <div class="tips-count"><%=tips_count%> Tips</div>
                	<div class="average-likes"><%=avg_likes%> Likes</div>
                
</div>
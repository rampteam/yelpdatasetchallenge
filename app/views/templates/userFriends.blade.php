	<% if(user.length === 0) { %>
		<h3 class="text-center">User not found</h3>
	<% } else if (user[0].hasOwnProperty('friends') === false || user[0].friends.length === 0) { %>
		<h3 class="text-center">This user has no friends</h3>
	<% } else { %>
		<% for( key in user[0].friends) { %>
			<div class="large-6 columns medium-centered left">
				<a href="javascript:void(0)" class="user-friend text-center medium-centered" data-id="<%=user[0].friends[key].user_id%>">
					<%=user[0].friends[key].name%>
				</a>
			</div>
		<% } %>
	<% } %>

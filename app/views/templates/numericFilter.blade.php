<div class="option numeric">
	<div class="numeric-filter-label eq<%=classes.eq%>">&#61;</div>  
	<input class="numeric-input search-filter-input<%=classes.eq%>" max="<%=max%>" type="number" value="<%=values.eq%>"/>
</div>
<div class="option numeric">
	<div class="numeric-filter-label lower lt<%=classes.lt%>">&lt;</div>  
	<input class="numeric-input search-filter-input<%=classes.lt%>" max="<%=max%>" type="number" value="<%=values.lt%>"/>
</div>
<div class="option numeric">
	<div class="numeric-filter-label lower lte<%=classes.lte%>">&le;</div>  
	<input class="numeric-input search-filter-input<%=classes.lte%>" max="<%=max%>" type="number" value="<%=values.lte%>"/>
</div>
<div class="option numeric">
	<div class="numeric-filter-label upper gt<%=classes.gt%>">&gt;</div>  
	<input class="numeric-input search-filter-input<%=classes.gt%>" max="<%=max%>" type="number" value="<%=values.gt%>"/>
</div>
<div class="option numeric">
	<div class="numeric-filter-label upper gte<%=classes.gte%>">&ge;</div>  
	<input class="numeric-input search-filter-input<%=classes.gte%>" max="<%=max%>" type="number" value="<%=values.gte%>"/>
</div>
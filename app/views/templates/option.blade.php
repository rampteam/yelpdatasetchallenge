<div class="search-filter
	<% if(typeof(rootClass) !== 'undefined') { %>
		<%=rootClass%>
	<% } %>">
	<div class="search-filter-label large-12 columns">
		<span><%=name%></span>
		<div class="arrow arrow-right right"></div>
	</div>
	<div class="search-filter-content large-12 columns">
		<div class="back">
			<div class="arrow-left left"></div>
			<span>Back</span>
		</div>
		<%=options%>
	</div>
</div>
@extends('layouts.users')

@section('errors')
@stop

@section('content')
	<div class="large-8 columns medium-centered">
		<fieldset>
			<legend>Edit</legend>
				{{ Form::open(array('action' => array('UsersController@update', $user->id), 'method' => 'put')) }}	
					{{ Form::label('firstName', 'First Name') }}
					{{ Form::text('firstName', $user->firstName, array('placeholder' => 'First Name')) }}
					{{ $errors->first('firstName', '<small class="error text-center" data-abide>:message</small>')}}

					{{ Form::label('lastName', 'Last Name') }}
					{{ Form::text('lastName', $user->lastName, array('placeholder' => 'Last Name')) }}
					{{ $errors->first('lastName', '<small class="error text-center" data-abide>:message</small>')}}

					{{Form::label('password', 'Current passowrd')}}
					{{Form::password('password', array('placeholder' => 'Current password' ))}}
					{{ $errors->first('password', '<small class="error text-center" data-abide>:message</small>')}}

					{{ Form::label('newpassword1', 'New passowrd') }}
					{{ Form::password('newpassword1', array('placeholder' => ' New password (min 6 characters)')) }}
					{{ $errors->first('newpassword1', '<small class="error text-center" data-abide>:message</small>')}}
					
					{{ Form::label('newpassword2', 'Retype new password') }}
					{{ Form::password('newpassword2', array('placeholder' => 'Retype new password')) }}
					{{ $errors->first('newpassword2', '<small class="error text-center" data-abide>:message</small>')}}
					

					<br/>
					{{ Form::submit('Update', array('class' => 'button')) }}
				{{ Form::close() }}
		</fieldset>
	</div>
@stop
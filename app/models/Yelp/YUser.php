<?php 
namespace Yelp\Model;

use \Jenssegers\Mongodb\Query\Builder as MQueryBuilder;

class YUser extends YAbstractModel
{
	
	const COLL_NAME = 'user';

	public static $fields = array("yelping_since", "votes", "review_count", "name", "user_id", "friends", "fans", "average_stars", "type", "compliments", "elite");
	
	public static $filter = array(
    	'regex' => array('name'),
    	'raw'   => array('votes'),
    	'exact' => array('average_stars', 'user_id', 'yelping_since'),
    	'range' => array('average_stars', 'review_count', 'yelping_since', 'fans')
    );

    public static $fieldType = array(
        'average_stars' => 'float',
        'review_count' 	=> 'float',
        'fans' 		    => 'int' 
    );

	/**
	 * Init model
	 * @param array $attributes
	 */
	public function __construct(array $attributes = array()) {
		parent::__construct($attributes);
	}

	public function reviews()
  {
    return $this->hasMany('\Yelp\Model\YReview', 'user_id', 'user_id');
  }

  public static function applyRawFilter($query, $column, $value)
  {
   if($column !== "votes") return ;

   $allowedKeys = array("funny", "useful", "cool");

   return static::applyNestedFilter($query, $column, $value, $allowedKeys);
 }
}
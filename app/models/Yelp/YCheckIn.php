<?php 
namespace Yelp\Model;

use \Jenssegers\Mongodb\Query\Builder as MQueryBuilder;

class YCheckIn extends YAbstractModel
{
	
	const COLL_NAME = 'checkin';

	public static $fields = array( "checkin_info", "type", "business_id");

	public static $filter = array(
        'regex' => array('text'),
        'exact' => array('user_id', 'business_id', 'date'),
        'range' => array('date')
    );

  public static $fieldType = array(
        
  );
    
	/**
	 * Init model
	 * @param array $attributes
	 */
	public function __construct(array $attributes = array()) {
		parent::__construct($attributes);
	}

	public function business()
    {
        return $this->belongsTo('Yelp\Model\YBusiness', 'business_id', 'business_id');
    }

    public static function applyRawFilter($query, $column, $value)
    {
       if($column !== "checkin_info") return ;

       $allowedKeys = array();

       foreach (rand(1, 23) as $hour) 
       {
       		foreach (rand(0, 6) as $day) 
       		{
       			$allowedKeys[] = "{$hour}-{$day}";
       		}
       }
      
       return static::applyNestedFilter($query, $column, $value, $allowedKeys);
    }

}
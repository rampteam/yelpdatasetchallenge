<?php 
namespace Yelp\Model;

class YReview extends YAbstractModel
{
	
	const COLL_NAME = 'review';

	public static $fields = array( "votes", "user_id", "review_id", "stars", "date", "text", "type", "business_id");
	
	public static $filter = array(
        'regex' => array('text'),
        'raw'   => array('votes'),
        'exact' => array('stars', 'user_id', 'business_id', 'date'),
        'range' => array('stars', 'date')
    );

    public static $fieldType = array(
        'stars' => 'float'
  	);

	/**
	 * Init model
	 * @param array $attributes
	 */
	public function __construct(array $attributes = array()) {
		parent::__construct($attributes);
	}

	public function business()
    {
        return $this->belongsTo('Yelp\Model\YBusiness', 'business_id', 'business_id');
    }

    public function user()
    {
        return $this->belongsTo('Yelp\Model\YUser', 'user_id', 'user_id');
    }
}
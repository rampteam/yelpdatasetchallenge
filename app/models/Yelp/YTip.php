<?php 
namespace Yelp\Model;

class YTip extends YAbstractModel
{
	
	const COLL_NAME = 'tip';

	public static $fields = array( "user_id", "text", "business_id", "likes", "date", "type");

	public static $filter = array(
        'regex' => array('text'),
        'exact' => array('user_id', 'business_id', 'date'),
        'range' => array('likes', 'date')
    );

    public static $fieldType = array(
        'likes' => 'int'
  	);

	/**
	 * Init model
	 * @param array $attributes
	 */
	public function __construct(array $attributes = array()) {
		parent::__construct($attributes);
	}

	public function business()
    {
        return $this->belongsTo('Yelp\Model\YBusiness', 'business_id', 'business_id');
    }

    public function user()
    {
        return $this->belongsTo('Yelp\Model\YBusiness', 'user_id', 'user_id');
    }
}
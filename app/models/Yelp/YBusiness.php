<?php 
namespace Yelp\Model;

class YBusiness extends YAbstractModel
{
	
	const COLL_NAME = 'business';


    public static $fields = array( "business_id", "full_address", "hours", "open", "categories", "city", "review_count", "name", "neighborhoods", "longitude", "state", "stars", "latitude", "attributes", "type");
	

    public static $filter = array(
        'regex' => array('name', 'city', 'state', 'categories'),
        'exact' => array('stars', 'review_count', 'business_id'),
        'range' => array('stars', 'review_count')
    );

    public static $fieldType = array(
        'stars'        => 'float',
        'review_count' => 'float' 
    );

    /**
	 * Init model
	 * @param array $attributes
	 */
	public function __construct(array $attributes = array()) 
    {
		parent::__construct($attributes);
	}

	public function checkins()
    {
        return $this->hasMany('\Yelp\Model\YCheckIn', 'business_id', 'business_id');
    }

    public function reviews()
    {
        return $this->hasMany('\Yelp\Model\YReview', 'business_id', 'business_id');
    }

    public function tips()
    {
        return $this->hasMany('\Yelp\Model\YTip', 'business_id', 'business_id');
    }
    
}



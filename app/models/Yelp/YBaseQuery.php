<?php 
namespace Yelp\Model;

use \Jenssegers\Mongodb\Query\Builder as MQueryBuilder;
use Yelp\Mongodb\Query\Builder as YelpQuery;
use Yelp\Mongodb\Eloquent\Builder as Builder;
use Exception;
use InvalidArgumentException;

class YBaseQuery extends \Moloquent {
    
    public static $filter = array(
        'regex' => array(),
        'raw'   => array() 
    );

    public static $fieldType = array(
       
    );
    
    protected static $operators = array(
            'eq' => '=',
            'gt' => '>',
            'gte' => '>=',
            'lt' => '<',
            'lte' => '<='
    );
 

    /**
     * A list of valid columns
     * @var unknown
     */
    public static $fields = array();


    /**
     * A list of valid columns
     * @var unknown
     */
    public static $except = array("_id");

    /**
     * This mirrors conncetion modl property
     * */
    const CONN = 'mongodb';
    
    /**
     * 
     * @param unknown $filters
     * @param $query
     */
    public static function applyFilter($query, $filters, $enforceFilter = false)
    {
       
        $toApply = static::matchFilters($filters);
        
        if((empty($toApply) || !is_array($toApply)) && $enforceFilter === true)
        {
            throw new InvalidArgumentException("Please specify some filters!");
            
        }
        
        if(!is_array($toApply)) return ;

        foreach ($toApply as $filterType => $fields) 
        {
            static::applyFilterByType($query, $filters, $filterType, $fields);
        }

    }

    protected static function applyFilterByType($query, $filters, $type, $fields)
    {   
        $method = "apply".ucfirst($type)."Filter";
        //late state binding
        
        $class = get_called_class();
        if(!method_exists($class, $method))
        {
            throw new \InvalidArgumentException("No filter method {$method} found!");  
        }

        foreach ($fields as $field ) 
        {   
           $value = array_get($filters, $field, null);
           if(empty($value)) continue;

           static::convertValues($field, $value);
        
           call_user_func($class."::".$method, $query, $field, $value);
        }

    }

    public static function applyRawFilter($query, $column, $value)
    {
        //every model should  overwrite this
    }

    public static function applyExactFilter($query, $column, $value)
    {
        if (is_array($value))
        {
            $query->whereIn($column, $value);
        } 
        else 
        {
            $query->where($column, '=', $value);
        }
    }

    public static function applyRegexFilter($query, $column, $value)
    {   
        $query->where($column, 'regex', new \MongoRegex("/{$value}.*/i"));
    }

    public static function applyRangeFilter($query, $column, $value)
    {   
        $operators = static::$operators;
        //value should contain some operators
      
        $operatorsDiff = array_intersect(array_keys($operators), array_keys($value));

        if(empty($operatorsDiff))
        {
             throw new \InvalidArgumentException("Unexpected value:".print_r($value, true)." for filter: {$column}!");
        }

        foreach ($operatorsDiff as $op) 
        {
            $query->where($column,  $operators[$op], $value[$op]);
        }
        
    }

    public static function applyNestedFilter($query, $column, $value, $allowedKeys = array())
    {
       $intersect = array_intersect($allowedKeys, array_keys($value));

       if(empty($intersect)) return ;

       foreach ($intersect as $type) 
       {    
            $typeVal = $value[$type];
            $valueType = static::getFilterValueType($typeVal);
            if($valueType === "range")
            {
                return static::applyRangeFilter($query, $column.$type, $typeVal);
            }else
            {
                return static::applyExactFilter($query, $column.$type, $typeVal);
            }
       }    
    }


    public static function applyFilterBasedOnValue($query, $column, $typeVal)
    {
        $valueType = static::getFilterValueType($typeVal);
        if($valueType === "range")
        {
            return static::applyRangeFilter($query, $column, $typeVal);
        }else
        {
            return static::applyExactFilter($query, $column, $typeVal);
        }
    }

    protected static function getFilterValueType($value)
    {   
        $operators = static::$operators;

        $intersect = array_intersect(array_keys($operators), array_keys($value));
        //could be range or values
        if(is_array($value) && !empty($intersect))
        {
            return "range";
        }

        return gettype($value);
    }


    
    /**
     * 
     * @param unknown $orderBy
     * @param QueryBuilder $query
     */
    public static function applyOrder($query, $orderBy)
    {   
        if(empty($orderBy)) return;
        
        $fields = static::getAllColumns();
        foreach ($orderBy as $column => $order){

            if(!in_array($column, $fields)) continue;

            if($order != 'ASC' && $order != 'DESC'){
                continue;
            }

            $query->orderBy($column, $order);
        }
    }
    
    /**
     * 
     * @param QueryBuilder $query
     * @param unknown $keyword
     * @param array $collumns
     */
    public static function applySearch($query, $keyword, array $collumns = array())
    {    
        if(empty($keyword))
        {
            return ;
        }
       
       foreach ($collumns as $column)
       {
            $query->where($column, 'regex', new \MongoRegex("/.*".$keyword.".*/i"))->get();
       }
    }


    /**
     * 
     * @param QueryBuilder $query
     * @param unknown $keyword
     * @param array $collumns
     */
    public static function applyTextSearch($query, $keyword, $column)
    {    
        if(empty($keyword)) return;
        $query->whereRaw(array($column => array('$text' => array('$search' => $keyword))));
    }
    
    
    /**
     * apply limit clause
     * @param QueryBuilder $query
     * @param unknown $limit
     */
    public static function applyLimit($query, $limit){
        if(!empty($limit) && is_numeric($limit))
        {   
            $limit = intval($limit);
            $cLimit = static::getBoundLimit($limit);
            $query->take($cLimit); 
        }
        
    }
    
    /**
     * apply offset clause
     * @param QueryBuilder $query
     * @param unknown $offset
     */
    public static function applyOffset($query, $offset)
    { 
        if(!empty($offset) && is_numeric($offset))
        {   
            $offset = intval($offset);
            $query->skip($offset); 
        }
    }


      /**
     * apply offset clause
     * @param QueryBuilder $query
     * @param unknown $offset
     */
    public static function getRows($query, $columns = null)
    {   
        if(!empty($columns))
        {   
           $columns = array_diff($columns, static::$except);
           return  $query->get($columns);

        }else
        {
            return $query->get();
        }

    }


    /**
     * apply offset clause
     * @param QueryBuilder $query
     * @param unknown $offset
     */
    public static function getFirstRow($query, $columns = null)
    {   
        if(!empty($columns))
        {   
           $columns = array_diff($columns, static::$except);
           return  $query->first($columns);

        }else
        {
            return $query->first();
        }

    }
    
    /**
     * Remove Eloquent query builder proxy
     * @return Ambigous <\Jenssegers\Mongodb\Query>
     */
    public static function query()
    {
        return parent::query()->getQuery();
    }

    /**
     * Remove Eloquent query builder proxy
     * @return 
     */
    public static function yelpQuery()
    {
        $instance = new static;

        $connection = $instance->getConnection();

        // Check the connection type
        if ($connection instanceof \Jenssegers\Mongodb\Connection)
        {
            $queryBuilder = new YelpQuery($connection);
        }else
        {
            $queryBuilder = parent::newBaseQueryBuilder();
        }

        $builder = new Builder($queryBuilder);

        $builder->setModel($instance);
        
        return $builder;
    }



    protected static function getDBName()
    {
        $conn = static::CONN;

        $dbConnections = \Config::get('database.connections');

        $mongodb = array_get($dbConnections, $conn, null);

        $ex = new \LogicException("Could not get mognodb database connection configuration!");;
        if($mongodb === null)
        {
            throw $ex; 
        }

        $dbName = array_get($mongodb, "database", null);

        if($dbName === null)
        {
            throw $ex;    
        } 

        return $dbName;
    }

    protected static function getCollName()
    {
        return static::COLL_NAME;
    }


    protected static function getDBColl()
    {
        return array(
            "dbName" => static::getDBName(),
            "collName" => static::getCollName()
        );
    }

    protected static function getAllColumns()
    {
        return static::$fields;
    }

    protected static function matchFilters($filters = array())
    {
        if(empty($filters)) return $filters;

        $toApply = array();

        foreach ($filters as $field => $value) 
        {
            $filterType = static::computeFilterType($field, $value);

            if($filterType === null) continue;
            if(!isset($toApply[$filterType]))
            {
                $toApply[$filterType] = array();
            }

            $toApply[$filterType][] = $field;
        }

        return $toApply;
    }

    protected static function computeFilterType($field, $value)
    {   
        $types = array();
        $filters = static::$filter;
        foreach ($filters as $type => $fields) 
        {
            if(in_array($field, $fields))
            {
                $types[] = $type;
            }
        }
        
        if(empty($types)) return null;

        $allowedValues = static::getFilterValuesAllowedType();
        $valueType = gettype($value);

        $validMatch = array();
        $isValid = false;
        foreach ($types as $type ) 
        {
            if(isset($allowedValues[$type]) && in_array($valueType, $allowedValues[$type]))
            {
                $isValid = true;
                $validMatch[] = $type;
            }
        }
        
        static::filterValidMatch($validMatch, $value);

        if($isValid === false)
        {
            throw new \InvalidArgumentException("Unexpected value: ".print_r($value, true)." for filter: {$field}!");
        } 
      
        //return first for now
        return array_shift($validMatch);

    }

    protected static function filterValidMatch(array &$match, $value)
    {   
             //if more than one is valid then what to do?
        if(count($match) > 1)
        {   
            //avoid raw use specialization
            $key = array_search('raw', $match);
            if($key !== false)
            {
                unset($match[$key]);
            }
        }

        //if 
        $sameType = array("range", "exact");
        if(count(array_intersect($sameType, $match)) == 2)
        {
            $valueType = static::getFilterValueType($value);
            if($valueType === "range")
            {
                $key = array_search("exact", $match);
                if($key !== false)
                {
                    unset($match[$key]);
                }
            }
        }
    }

    protected static function getFilterValuesAllowedType()
    {   
        return  array(
            'regex' => array('integer', 'double', 'string'),
            'raw'   => array('integer', 'double', 'string', 'array'),
            'exact' => array('integer', 'double', 'string', 'array'),
            'range' => array('array')
        );
    }

    protected static function convertValues($field, &$value)
    {
        $fieldType = static::$fieldType;

        if(!in_array($field, array_keys($fieldType))) return ;

        $mapToFunc = array(
            "float" => "floatval",
            "int"   => "intval"
        );

        $exType = $fieldType[$field];

        if(!isset($mapToFunc[$exType])) return ;

        $conversionFunc = $mapToFunc[$exType];

        if(is_array($value))
        {
            $value = array_map($conversionFunc, $value);
        }else
        {
            $value = call_user_func($conversionFunc, $value);
        }
    }

    public static function getValidFiltersOnly($filters = array())
    {   
        if(empty($filters) || !is_array($filters)) return $filters;

        $declaredFilters = static::getDeclaredFilters();

        $validFilters = array();

        //there might be some name classhes so append the collection name as namespace
        $collName = static::getCollName();

        foreach ($filters as $field => $value) 
        {   
            $withPrefix = $field;
            if(starts_with($field, $collName))
            {
                $withPrefix = substr($field, strlen($collName));
            }

            if (in_array($withPrefix, $declaredFilters) == true) 
            {
                $validFilters[$withPrefix] = $value;
            }
            
        }

        return $validFilters;
    }

    public static function getDeclaredFilters()
    {
        $declaredFilters = static::$filter;
        $filterKeys = array();
        foreach ($declaredFilters as $filterType => $filterFields) 
        {
            $filterKeys = array_merge($filterKeys, $filterFields);
        }

        return array_unique($filterKeys);
    }

    public static function haveValidFilter($filters = array())
    {
    	$validFilters = static::getValidFiltersOnly($filters);

    	return !empty($validFilters);
    }


    public static function getValidColumnData($data, $skip = array())
    {
        $allFields = static::getAllColumns();

        $validData = array();

        if(empty($data) || !is_array($data))
        {
            return $validData;
        }

        foreach ($data as $key => $value) 
        {   

           if(in_array($key, $skip) || in_array($key, $allFields))
           {
                $validData[$key] = $value;
           }
        }

        return $validData;

    }

    protected static function loadBelongsTo($rows, $fk, $pk, $model, $dataKey)
    {   
        if(empty($rows))
        {
            return $rows;
        }

        $ids = array_pluck($rows, $fk);

        if(empty($ids))
        {
            return $rows;
        }

        $query = $model::query();

        $model::applyFilter($query, array($pk => $ids));

        $parents = $query->get();

        if(empty($parents) || !is_array($parents)) return $rows;

        $assocData = \YUtil::makeAssocByKey($parents, $pk);
        //map data
        foreach ($rows as $nIndex => $row) 
        {
            $parentData = empty($assocData[$row[$fk]]) ? array() : $assocData[$row[$fk]];

            $rows[$nIndex][$dataKey] = $parentData;
        }

        return $rows;
    }

    public static function convertMongoDateToText(array &$rows, $columns)
    {   

        if(!is_array($columns))
        {
            $columns = array($columns);
        }

        //some optimization steps
        if(empty($rows))
        {
            return;
        }

        $firstRow = current($rows);

        $validate = array_intersect(array_keys($firstRow), $columns);

        if(empty($validate))
        {
            return;
        }

        foreach ($rows as &$row) 
        {
            static::convertMongoDateFieldsToText($row, $columns);
        }
    }

    public static function convertMongoDateFieldsToText(&$row, array $columns)
    {
        $check = array_intersect(array_keys($row), $columns);

        if(empty($check)) return ;

        foreach ($check as $key) 
        {
            if($row[$key] instanceof \MongoDate)
            {
                $row[$key] = date('m/d/Y', $row[$key]->sec); 
            }
        }  
    }

    public static function getBoundLimit($limit)
    {	
    	$limit = $limit > 1000 ? 1000 : $limit;

    	return $limit;
    }

}
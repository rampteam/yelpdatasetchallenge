<?php

class AuthController extends BaseController {
	
	protected $layout  = 'layouts.users';


	public function __construct() 
	{	

		$this->beforeFilter('csrf', array('on' => 'post'));	
		$this->beforeFilter('yauth.app', array('except' => array('getLogin', 'postLogin')));
		$this->beforeFilter('yauth.setinfo', array('only' => array('getLogin', 'postLogin')));
		$this->beforeFilter('yauth.dennied', array('only' => array('create', 'store')));
		
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getLogin()
	{
		$this->layout->content = View::make('users.login');
	}

	public function postLogin()
	{
		// get input data
		$credentials = Input::all();
		$credentials = array_only($credentials, array('email', 'password'));

		//validation
		$validation = new Yelp\Validators\Login($credentials);
		if ($validation->passes())
		{
			$user = new Yelp\Auth\User\DatabaseUser($credentials);

			if($user->authenticate()) 
			{
				$data =	YAuth::login($user);
				Session::put('token', $data['token']);
				return Redirect::to('/');
			}
		}
		return Redirect::back()->withErrors('Invalid Email or Password');
	}

	public function getLogout()
	{
		YAuth::logout();
		return Redirect::action('AuthController@getLogin');
	}

}

<?php

namespace Api;

use Yelp\Model\YCheckIn;

class YCheckInController extends BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // get client options
        $options = $this->getInputParams(array('filter', 'limit', 'offset', 'orderBy', 'columns'));
        // create query
        $query = YCheckIn::query();
        
        // apply filter options
        YCheckIn::applyFilter($query, $options['filter']);
        
        // set order by
        YCheckIn::applyOrder($query, $options['orderBy']);
        
        // apply limit and offset
        YCheckIn::applyLimit($query, $options['limit']);
        YCheckIn::applyOffset($query, $options['offset']);
        
        $columns = is_array($options['columns']) ? $options['columns'] : array();
        // execute query
        $reviews = $query->get($columns);
        
        //dump(\DB::connection('mongodb')->getQueryLog());
        return \Response::make($reviews, \HttpStatus::STATUS_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        // parse client data
        $options = $this->getInputParams(array('withBusiness'));
        
        $relations = array();
        
        if(!is_null($options['withBusiness'])){
            $relations[] = 'business';
        }

        $review = YCheckIn::with($relations)->where('_id', $id)->first();

        if (empty($review)) {
             return \Response::make(array(
                'Message' => 'Check in not found',
            ), \HttpStatus::ERROR_NOT_FOUND);
        }

        return \Response::make($review, \HttpStatus::STATUS_OK);
    }


}
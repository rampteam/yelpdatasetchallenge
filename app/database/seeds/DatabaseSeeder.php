<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
		User::create(array('email' => 'admin@example.dev', 'password' => Hash::make('admin'), 'firstName' => 'FisrtName', 'lastName' => 'LastName'));
		// $this->call('UserTableSeeder');
	}

}

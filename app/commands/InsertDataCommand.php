<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Illuminate\Support\Facades\Config;
use Yelp\MongoDB\DataInsertion\Insert as MongoInsert;

class InsertDataCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'app:batch:upsert';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command used to upsert json data.';

    /**
     * Create a new command instance.
     *
     * @return \InsertDataCommand
     */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		//
        $options  = $this->option();

        $fileDump = $options['file'];

        $mc = Config::get('database.connections.mongodb');

        $insert = new MongoInsert( $fileDump, $mc);

        $response = $insert->insertData();

        extract($response);

        if(isset($status) && is_array($status))
        {
        	$this->displayStatus($status);
        }
	}


	protected function displayStatus($statusData)
	{
		foreach ($statusData as $msgData) 
		{
			$method = $msgData["type"];
			if(method_exists($this, $method))
			{
				$this->{$method}($msgData["msg"]);
			}
		}
	}
	
	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(

		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('file', 'f', InputOption::VALUE_REQUIRED, 'The data file we need to upsert', null)
		);
	}

}

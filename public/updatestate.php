<?php 

set_time_limit(0);


  $stateAbbr = array(
         "Alabama"       => "AL",
         "Alaska"        => "AK",
         "Arizona"       => "AZ",
         "Arkansas"      => "AR",
         "California"    => "CA",
         "Colorado"      => "CO",
         "Connecticut"   => "CT",
         "Delaware"      => "DE",
         "Florida"       => "FL",
         "Georgia"       => "GA",
         "Hawaii"        => "HI",
         "Idaho"         => "ID",
         "Illinois"      => "IL",
         "Indiana"       => "IN",
         "Iowa"          => "IA",
         "Kansas"        => "KS",
         "Kentucky"      => "KY",
         "Louisiana"     => "LA",
         "Maine"         => "ME",
         "Maryland"      => "MD",
         "Massachusetts" => "MA",
         "Michigan"      => "MI",
         "Minnesota"     => "MN",
         "Mississippi"   => "MS",
         "Missouri"      => "MO",
         "Montana"       => "MT",
         "Nebraska"      => "NE",
         "Nevada"        => "NV",
         "New Hampshire" => "NH",
         "New Jersey"    => "NJ",
         "New Mexico"    => "NM",
         "New York"      => "NY",
         "North Carolina"=> "NC",
         "North Dakota"      => "ND",
         "Ohio"      => "OH",
         "Oklahoma"      => "OK",
         "Oregon"        => "OR",
         "Pennsylvania"      => "PA",
         "Rhode Island"      => "RI",
         "South Carolina"        => "SC",
         "South Dakota"      => "SD",
         "Tennessee"     => "TN",
         "Texas"     => "TX",
         "Utah"      => "UT",
         "Vermont"       => "VT",
         "Virginia"      => "VA",
         "Washington"        => "WA",
         "West Virginia"     => "WV",
         "Wisconsin"     => "WI",
         "Wyoming"       => "WY"
         );


    $mongoClient = new MongoClient();

    $collection = $mongoClient->selectCollection("yelp", "business");

    foreach ($stateAbbr as $fullName => $state) 
    {
        $collection->update(array("state" => $state), array('$set' => array("state" => strtolower($fullName))), array("multiple"=> true, "socketTimeoutMS" => 1, "w" => 0));

    } 
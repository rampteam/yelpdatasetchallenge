var Yelp = Yelp || {};
Yelp.Models = Yelp.Models || {};

! function() {

  "use strict";

  /**
   * Review Model
   */

  Yelp.Models.YCity = Yelp.Models.BaseModel.extend({
    defaults: {
      name: '',
    },
    initialize: function() {
      // bind this to methods
      _.bindAll(this, 'getSvgInfo');
    },

    getSvgInfo: function() {
      return {
        name: this.get('name'),
        children: [{
          name: 'Users',
          children: [{
            name: 'Users',
            size: parseInt(Math.random() * 100, 10),
          }],
        }, {
          name: 'Businesses',
          children: [{
            name: 'Businesses',
            size: parseInt(Math.random() * 100, 10),
          }],
        }],
      };
    },
  });
}();
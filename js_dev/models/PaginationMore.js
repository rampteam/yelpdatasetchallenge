var Yelp = Yelp || {};
Yelp.Models = Yelp.Models || {};

! function() {

	"use strict";

	/**
	 * Review Model
	 */

	Yelp.Models.PaginationMore = Yelp.Models.BaseModel.extend({
		defaults: {
			currentPage: 1,
			itemsPerPage: 10,
      eventName: '',
		},
	});
}();

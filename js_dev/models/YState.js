var Yelp = Yelp || {};
Yelp.Models = Yelp.Models || {};

! function() {

  "use strict";

  /**
   * Review Model
   */

  Yelp.Models.YState = Yelp.Models.BaseModel.extend({
    model: Yelp.Models.YCity,
    modelKey: 'cities',
    models: [],

    defaults: {
      short_name: '',
      long_name: '',
    },

    initialize: function(attributes, options) {
      // bind this to methods
      _.bindAll(this, 'getSvgInfo');
      // set cities models
      this.models = [];
      if (_.has(attributes, this.modelKey)) {
        _.each(attributes[this.modelKey], function(value, key, list) {
          this.models.push(new Yelp.Models.YCity(value));
        }, this);
        delete attributes[this.modelKey];
      }
      Backbone.Model.prototype.initialize.call(attributes, options);
    },

    getSvgInfo: function() {
      var data = {
        name: this.get('short_name'),
        children: []
      };
      _.each(this.models, function(model, key, list) {
        data.children.push(model.getSvgInfo());
      }, this);
      return data;
    }
  });
}();
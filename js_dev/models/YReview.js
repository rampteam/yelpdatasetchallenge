var Yelp = Yelp || {};
Yelp.Models = Yelp.Models || {};

! function() {

	"use strict";

	/**
	 * Review Model
	 */

	Yelp.Models.YReview = Yelp.Models.BaseModel.extend({

		defaults: {
      user_name: '',
      business: {},
			user: {},
			stars: 0,
			text: '',
			date: '',
			votes: {},
		},
	});
}();

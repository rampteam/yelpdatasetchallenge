var Yelp = Yelp || {};
Yelp.Models = Yelp.Models || {};

! function() {

	"use strict";

	/**
	 * Check in Model
	 */

	Yelp.Models.YCheckin = Yelp.Models.BaseModel.extend({

		idAttribute: 'business_id',
		defaults: {
			business_id: '',
			checkin_info: {},
		}

	});
}();

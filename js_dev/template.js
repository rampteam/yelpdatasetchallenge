var Yelp = Yelp || {};

Yelp.template = function(id) {
	return _.template($('#' + id).html());
};

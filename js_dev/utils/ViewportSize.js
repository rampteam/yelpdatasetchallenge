var Yelp = Yelp || {};
Yelp.Utils = Yelp.Utils || {};

! function() {

	"use strict";

	Yelp.Utils.ViewportSize = function() {
		return {
			w: window.innerWidth,
			h: window.innerHeight,
			x: window.pageXOffset,
			y: window.pageYOffset
		};
	};
}();

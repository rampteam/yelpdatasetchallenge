String.prototype.isNumeric = function() {
    return !isNaN(parseFloat(this)) && isFinite(this);
};

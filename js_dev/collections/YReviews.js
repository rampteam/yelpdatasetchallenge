var Yelp = Yelp || {};
Yelp.Collections = Yelp.Collections || {};

!(function() {

  "use strict";

  Yelp.Collections.YReviews = Backbone.Collection.extend({
    model: Yelp.Models.YReview,
    url: '/api/yreviews',
  });
})();
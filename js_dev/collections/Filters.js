var Yelp = Yelp || {};
Yelp.Collections = Yelp.Collections || {};

!(function() {

  "use strict";

  Yelp.Collections.Filters = Backbone.Collection.extend({
    model: Yelp.Models.Filter,

    findByNameAndType: function(name, type) {
      return this.find(function(model) {
        return model.get('name') == name && model.get('type') == type;
      });
    },
  });
})();
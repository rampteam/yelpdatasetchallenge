var Yelp = Yelp || {};
Yelp.Collections = Yelp.Collections || {};

!(function() {

  "use strict";

  Yelp.Collections.YBusiness = Backbone.Collection.extend({
    model: Yelp.Models.YBusiness,
    url: 'api/ybusinesses',

    getSvgJson: function() {
      var svgJson = {
        name: 'Businesses',
        children: []
      };
      this.each(function(el, index) {
        svgJson.children.push(el.getSvgInfo());
      });
      if (Object.keys(svgJson.children).length === 0) {
        return {};
      }
      return svgJson;
    },
  });
})();
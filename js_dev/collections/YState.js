var Yelp = Yelp || {};
Yelp.Collections = Yelp.Collections || {};

!(function() {

	"use strict";

	Yelp.Collections.YState = Backbone.Collection.extend({
		model: Yelp.Models.YState,
		url: 'api/ybusinesses/states',
    events: {
      'reset': 'reset',
    },
	});
})();

var Yelp = Yelp || {};
Yelp.Collections = Yelp.Collections || {};

!(function() {

  "use strict";

  Yelp.Collections.YUsers = Backbone.Collection.extend({
    model: Yelp.Models.YUser,
    url: 'api/yusers/fgraph',
    edges: [],

    getGraphJson: function() {
      var graphJson = {
        'nodes' : [],
        'edges' : this.edges,
      };
      this.each(function(el, index) {
        graphJson.nodes.push(el.getGraphJson());
      });
      return graphJson;
    },

    set: function (models, options) {
      if(models !== undefined && models.hasOwnProperty('users'))
      {
        this.reset();
        this.edges = models.links;
        _.each(models.users, function(value, key, list) {
          this.models.push(new Yelp.Models.YUser(value));
        }, this);
      }
      else
      {
        this.edges = [];
        Yelp.Collections.YUsers.__super__.set.call(this);
      }
    }
  });
})();

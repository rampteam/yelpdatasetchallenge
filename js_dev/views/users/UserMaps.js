var Yelp = Yelp || {};
Yelp.Views = Yelp.Views || {};

! function() {

  "use strict";

  Yelp.Views.UserMaps = Yelp.Views.BaseView.extend({
    tagName: 'div',
    //
    className: 'map-user-canvas',
    id : 'userMap',

    infoTemplate : Yelp.template('userMapTooltip'),

    events: {
      'click .b-name' : 'redirectToBusiness'
    },

    initialize: function(options) {
      Yelp.Views.UserMaps.__super__.initialize.call(this);
      this.options = options || {};
      this.$content = $("#userMapModal");
      
      _.bindAll(this, 'render', 'show');
      
    },


    render: function() {
      this.$content.empty();
      this.$content.append(this.$el.empty());
      this.addMarkers();
      if (this.changed) {
        this.$el.empty();
      } else {
        this.$el.show();
        Yelp.Loading.close();
      }

      this.delegateEvents();
    },

    hide: function() {
      this.$el.hide();
    },  
    addMarkers: function()
    {
      var map = this.getGMapObject();

      var self = this;

      this.collection.each(function(model) {
        var coord = model.get('coord');
        try {
   			var contentInfo = self.getInfoContent(model);
        	map.addMarker({
          		lat: coord[1], 
          		lng: coord[0],
          		infoWindow: {
          		  content: contentInfo
          		}
       	 	}, this);
		}
		catch (e) {
   			//
		}
     
      });
    },

    getInfoContent: function(model)
    { 
      return this.infoTemplate({
        name: model.get('name'),
        business_id: model.get('business_id'),
        stars: model.get('avg_stars'),
        review_count: model.get('nr_reviews'),
        tips_count: model.get('nr_tips'),
        avg_likes: model.get('avg_likes'),
      });
    },

    show: function() {
      this.changed = false;
      this.$content.empty();

      if(this.options.user_id)
      {
        this.collection.fetch({
          data: 'user_id=' + this.options.user_id,
          success: this.render
        });
      }
      
    },

    getMapCoord: function (){
      //defaults Center of US
      var settings = {
        lat: 39.828127,
        lng: -98.579404
      };

      if(this.collection.length)
      {
        var firstModel = this.collection.at(0);
        var coord = firstModel.get('coord');
        settings.lat = coord[1];
        settings.lng = coord[0];
      }

      return settings;

    },

    redirectToBusiness: function(ev){
      var businessId = $(ev.currentTarget).attr('data-businessId');
      if(businessId)
      {
        Yelp.Router.setRoute('ybusinesses/' + businessId);
      }
    },

    getGMapObject: function(){
      var coord = this.getMapCoord();

      var map = new GMaps({
       div: '#' + this.id,
       zoom: 8,
       lat: coord.lat,
       lng: coord.lng
     });

      return map;
    }

  });
}();
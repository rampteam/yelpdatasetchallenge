var Yelp = Yelp || {};
Yelp.Views = Yelp.Views || {};

! function() {

  "use strict";

  Yelp.Views.IndexReview = Yelp.Views.BaseView.extend({
    tagName: 'div',
    className: 'row',
    // more button
    $more: null,
    moreModel: null,
    // load template from master.blade.php
    template: Yelp.template('reviewTemplate'),
    // constructor method
    initialize: function(models, options) {
      // instantiate Pagination Model
      this.moreModel = new Yelp.Models.PaginationMore({
        eventName: 'yreviewsIndexMore'
      });
      // call parent constructor
      Yelp.Views.IndexReview.__super__.initialize.call(this);
      // listen to yreview url event
      Yelp.Events.on('yreviews:index', this.show, this);
      // bind current object to methods
      _.bindAll(this, 'render', 'show');
      // bind event for more button
      Yelp.Events.on(this.moreModel.get('eventName'), this.paginate, this);
    },

    show: function(query) {
      // reset the pagination model
      this.moreModel.set('currentPage', 1);
      // prevent concurent loading
      this.changed = false;
      // // empty the conent
      this.$content.empty();
      // // put this element in page
      this.$content.append(this.$el.empty());
      // // ajax call to server
      query = Yelp.Router.getCurrentQueryObject() || {};
      query.limit = 10;
      query.withUser = 1;
      query.withBusiness = 1;
      query = $.param(query);
      this.collection.fetch({
        data: query,
        // ajax success callback
        success: this.render
      });
    },

    paginate: function() {
      var self = this;
      // get last page and add 1 to be the page we want to fetch from api
      var page = this.moreModel.get('currentPage') + 1;
      // calculate offset for this page
      var offset = (page - 1) * this.moreModel.get('itemsPerPage') + 1;
      if (offset > self.$el.children().length) {
        return;
      }
      Yelp.Loading.open();
      var limit = this.moreModel.get('itemsPerPage');
      // get current filters and set limit and offset
      var currentFilters = Yelp.Router.getCurrentQueryObject() || {};
      currentFilters.limit = limit;
      currentFilters.offset = offset;
      currentFilters.withUser = 1;
      currentFilters.withBusiness = 1;
      this.collection.fetch({
        // get next page data and append it to current data
        data: currentFilters,
        remove: false,
        success: function() {
          // empty the page and render all elements again

          self.$content.empty();
          self.$content.append(self.$el.empty());
          self.render();
          // get last element from before pagination
          var lastElement = self.$el.children(':nth-child(' + (offset - 1) + ')');
          // scroll window to last element before pagination event
          $(window).scrollTop(lastElement.offset().top);
          // set current page
          self.moreModel.set('currentPage', page);
        },
        always: function() {
          Yelp.Loading.close();
        }
      });
    },

    render: function() {
      // iterate through collection
      this.collection.each(function(model) {
        // create a view for each model
        var view = new Yelp.Views.Review({
          model: model,
        });
        // append the rendered view to this view
        this.$el.append(view.render().$el);
      }, this);

      this.$more = new Yelp.Views.PaginationMore({
        model: this.moreModel
      });
      // append more button to dom
      this.$el.append(this.$more.render().$el);

      // check if another page was loaded in the meantime ( changed is set to true in BaseView.js)
      if (this.changed) {
        this.$el.empty();
      } else {
        // show this element
        this.$el.show();
        Yelp.Loading.close();
      }
      // used for method chaining
      return this;
    },
  });
}();
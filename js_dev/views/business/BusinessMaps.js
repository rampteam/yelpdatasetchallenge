var Yelp = Yelp || {};
Yelp.Views = Yelp.Views || {};

! function() {

  "use strict";

  Yelp.Views.BusinessMaps = Yelp.Views.BaseView.extend({
    tagName: 'div',
    // 
    id: 'businessMap',
    className: 'map-canvas',

    infoTemplate: Yelp.template('businessMapTooltip'),

    events: {
      'click .b-name': 'redirectToBusiness'
    },

    initialize: function() {
      Yelp.Views.BusinessMaps.__super__.initialize.call(this);
      _.bindAll(this, 'render');
      Yelp.Events.on('ybussinesses:maps', this.show, this);
      _.bindAll(this, 'render', 'show');
    },


    render: function() {
      this.$content.empty();
      this.$content.append(this.$el.empty());
      this.addMarkers();
      if (this.changed) {
        this.$el.empty();
      } else {
        this.$el.show();
        Yelp.Loading.close();
      }

      this.delegateEvents();
    },

    addMarkers: function() {
      var map = this.getGMapObject();

      var self = this;

      this.collection.each(function(model) {
        var coord = model.get('coord');
        var contentInfo = self.getInfoContent(model);

        try {
          map.addMarker({
            lat: coord[1],
            lng: coord[0],
            title: model.get('name'),
            infoWindow: {
              content: contentInfo
            }
          }, this);
        } catch (e) {

        }

      });
    },

    getInfoContent: function(model) {
      return this.infoTemplate({
        name: model.get('name'),
        business_id: model.get('business_id'),
        stars: model.get('stars'),
        review_count: model.get('review_count'),
      });
    },

    show: function(query) {
      this.changed = false;
      this.$content.empty();
      this.collection.fetch({
        data: query,
        success: this.render
      });
    },

    getMapCoord: function() {
      //defaults Center of US
      var settings = {
        lat: 39.828127,
        lng: -98.579404
      };

      if (this.collection.length) {
        var firstModel = this.collection.at(0);
        var coord = firstModel.get('coord');
        settings.lat = coord[1];
        settings.lng = coord[0];
      }

      return settings;

    },

    redirectToBusiness: function(ev) {
      var businessId = $(ev.currentTarget).attr('data-businessId');
      if (businessId) {
        Yelp.Router.setRoute('ybusinesses/' + businessId);
      }
    },

    getGMapObject: function() {
      var coord = this.getMapCoord();

      var map = new GMaps({
        // div: '#businessMap',
        div: '#' + this.id,
        zoom: 8,
        lat: coord.lat,
        lng: coord.lng
      });

      return map;
    }

  });
}();
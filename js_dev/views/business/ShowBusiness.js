var Yelp = Yelp || {};
Yelp.Views = Yelp.Views || {};

! function() {

  "use strict";

  Yelp.Views.ShowBusiness = Yelp.Views.BaseView.extend({
    model: Yelp.Models.YBusiness,
    tagName: 'div',
    // 
    template: Yelp.template('businessTemplate'),
    imageUrl: null,

    initialize: function() {
      Yelp.Views.ShowBusiness.__super__.initialize.call(this);
      Yelp.Events.on('ybussinesses:show', this.show, this);
      _.bindAll(this, 'show', 'render', 'getTemplate', 'bindEvents', 'equalizeAditionalInfo');
    },

    show: function(id) {
      this.changed = false;
      var self = this;
      this.$content.empty();
      self.$el.empty();
      this.model.set('business_id', id);
      this.model.fetch({
        success: function() {
          self.$content.append(self.$el.hide());
          self.render();
          if (!self.changed) {
            self.$content.append(self.$el);
            self.$el.show();
            Yelp.Loading.close();
            _.delay(self.equalizeAditionalInfo, 200);
          } else
            self.$el.empty();
        },
      });
    },

    equalizeAditionalInfo: function() {
      var $objKeys = this.$('.aditional-object-key');
      if ($objKeys.length === 0) {
        _.delay(this.equalizeAditionalInfo, 200);
      } else {
        $objKeys.each(function(index, el) {
          var $this = $(el);
          $this.height($this.siblings('.aditional-object-value').height());
        });
      }
 
    },

    render: function() {
      var self = this;
      $.getJSON('https://ajax.googleapis.com/ajax/services/search/images?callback=?', {
        q: this.model.get('city') + ' ' + this.model.get('name'),
        v: '1.0',
      }, function(json, textStatus) {
        if (json.responseStatus == 200) {
          self.url = json.responseData.results[0].url;
        } else {
          self.url = 'static/images/no-image.png';
        }
        self.$el.html(self.getTemplate());
        self.bindEvents();
      });
      return this;
    },

    getTemplate: function() {
      this.buildCategories();
      return this.template({
        name: this.model.get('name'),
        categories: this.buildCategories(),
        stars: this.model.get('stars'),
        review_count: this.model.get('review_count'),
        longitude: this.model.get('coord')[0],
        latitude: this.model.get('coord')[1],
        full_address: this.model.get('full_address'),
        city: this.model.get('city'),
        state: this.model.get('state'),
        staticImage: this.url,
        hours: this.model.get('hours'),
        attributes: this.model.get('attributes'),
        cityRoute: Yelp.Router.buildRoute('#ybusinesses', {
          filter: {
            city: this.model.get('city'),
            state: this.model.get('state'),
          },
          limit: 500,
        }),
        stateRoute: Yelp.Router.buildRoute('#ybusinesses', {
          filter: {
            state: this.model.get('state'),
          },
          limit: 500,
        }),
      });

    },

    buildCategories: function() {
      var $categories = $('<div>');
      var categories = this.model.get('categories');
      for (var i = 0; i < categories.length; i++) {
        $('<a>')
          .attr('href', '#')
          .addClass('business-category')
          .html(categories[i]).appendTo($categories);
        $('<i>').html(', ').appendTo($categories);
      }
      $categories.children().last().remove();
      return $categories.html();
    },

    bindEvents: function() {
      this.categoryEvent();
      this.reviewsEvent();
      this.mapTrigger();
    },

    mapTrigger: function(){
      var self = this;
      this.$el.find('.business-map-trigger').click(function(event) {
        Yelp.Router.setRoute('ybusinesses/maps', {
          filter: {
            business_id: self.model.get('business_id'),
          },
        });
      });
    },

    reviewsEvent: function() {
      var self = this;
      this.$el.find('#businessReviews').click(function(event) {
        Yelp.Router.setRoute('yreviews', {
          filter: {
            business_id: self.model.get('business_id'),
          },
        });
      });
    },

    categoryEvent: function() {
      var self = this;
      self.$el.find('.business-category').click(function(event) {
        event.stopPropagation();
        event.preventDefault();
        Yelp.Router.setRoute('ybusinesses', {
          filter: {
            categories: this.innerHTML
          },
          limit: 100,
        })
      });
    },
  });
}();
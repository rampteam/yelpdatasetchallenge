var Yelp = Yelp || {};
Yelp.Views = Yelp.Views || {};

! function() {

  "use strict";

  Yelp.Views.IndexCircleStatesView = Yelp.Views.BaseView.extend({
    tagName: 'div',
    margin: 20,
    // svg 
    d3: null,
    color: null,
    pack: null,
    // 
    routing: false,

    initialize: function(models, options) {
      Yelp.Events.on('index:circle', this.show, this);
      Yelp.Views.IndexCircleStatesView.__super__.initialize.call(this);

      _.bindAll(this, 'render', 'buildSvg', 'setNewRoute');
      this.setSvgInfo();
    },

    setSvgInfo: function() {
      this.color = d3.scale.linear()
        .domain([-1, 5])
        .range(['hsl(152,80%,80%)', 'hsl(228,30%,40%)'])
        .interpolate(d3.interpolateHcl);

      // containement to represent the hierarchy, each element size will be determined by children size (leaf size eventualy)
      this.pack = d3.layout.pack()
        .padding(2)
        .size([this.diameter - this.margin, this.diameter - this.margin])
        .value(function(d) {
          return d.size;
        });
    },

    show: function() {
      this.changed = false;
      this.collection.fetch({
        success: this.render
      });
    },

    render: function() {
      this.$content.empty();
      this.$content.append(this.$el.empty());
      this.buildSvg();
      if (this.changed) {
        this.$el.empty();
      } else {
        this.$el.show();
        Yelp.Loading.close();
      }
      return this;
    },

    buildSvg: function() {
      // build svg json
      var self = this;
      var svgJson = this.getSvgJson();
      var nodes = this.pack.nodes(svgJson);
      var focus = svgJson;
      var view;

      // create svg and append a container
      this.svg = d3.select(self.el).append('svg')
        .attr('width', self.diameter)
        .attr('height', self.diameter)
        .append('g')
        .attr('transform', 'translate(' + self.diameter / 2 + ',' + self.diameter / 2 + ')');

      var circle = this.svg.selectAll("circle")
        .data(nodes)
        .enter()
        .append('circle')
        .attr('class', function(node) {
          if (node.parent) {
            if (node.children)
              return 'node';
            return 'node node--leaf';
          }
          return 'node node--root';
        })
        .style("fill", function(node) {
          if (node.children)
            return self.color(node.depth);
          return null;
        })
        .on("click", function(node) {
          if (node.depth === 3) {
            self.setNewRoute(node);
          }
          if (focus !== node) {
            zoom(node);
            d3.event.stopPropagation();
          }
        });

      var text = this.svg.selectAll("text")
        .data(nodes)
        .enter()
        .append("text")
        .attr("class", "label")
        .style("fill-opacity", function(node) {
          return node.parent === svgJson ? 1 : 0;
        })
        .style("display", function(node) {
          return node.parent === svgJson ? null : "none";
        })
        .text(function(node) {
          return node.name;
        });

      var node = this.svg.selectAll("circle,text");

      d3.select(self.el)
        .on("click", function() {
          zoom(svgJson);
        });

      zoomTo([svgJson.x, svgJson.y, svgJson.r * 2 + self.margin]);

      function zoom(node) {
        var focus0 = focus;
        focus = node;
        var transition = d3.transition()
          .duration(d3.event.altKey ? 7500 : 750)
          .tween("zoom", function(node) {
            var i = d3.interpolateZoom(view, [focus.x, focus.y, focus.r * 2 + self.margin]);
            return function(t) {
              zoomTo(i(t));
            };
          });

        transition.selectAll("text")
          .filter(function(node) {
            return node.parent === focus || this.style.display === "inline";
          })
          .style("fill-opacity", function(node) {
            return node.parent === focus ? 1 : 0;
          })
          .each("start", function(node) {
            if (node.parent === focus) this.style.display = "inline";
          })
          .each("end", function(node) {
            if (node.parent !== focus) this.style.display = "none";
          });
      }

      function zoomTo(v) {
        var k = self.diameter / v[2];
        view = v;
        node.attr("transform", function(d) {
          return "translate(" + (d.x - v[0]) * k + "," + (d.y - v[1]) * k + ")";
        });
        circle.attr("r", function(d) {
          return d.r * k;
        });
      }

      return this;
    },

    getSvgJson: function() {
      var svgJson = {
        "children": []
      };
      this.collection.each(function(el, index) {
        svgJson.children.push(el.getSvgInfo());
      });
      return svgJson;
    },

    setNewRoute: function(node) {
      if (this.routing === false && node.depth === 3) {
        this.routing = true;
        var state = encodeURIComponent(node.parent.parent.name);
        var city = encodeURIComponent(node.parent.name);
        var route = '';
        var filters = {
          filter: {
            city: city,
            state: state,
          }
        };
        if (node.name == 'Businesses') {
          route = 'ybusinesses';
           filters.limit = 250;
        } else if (node.name == 'Users') {
          route = 'yusers';
          filters.orderBy = {
            'review_count': 'DESC',
          };
          filters.limit = 250;
        }
        Yelp.Router.setRoute(route, filters);
        this.routing = false;
      }
    },

  });
}();
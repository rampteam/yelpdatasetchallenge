var Yelp = Yelp || {};
Yelp.Views = Yelp.Views || {};

! function() {

  "use strict";

  Yelp.Views.Filters = Yelp.Views.BaseView.extend({
    tagName: 'div',
    contentId: '',
    template: Yelp.template('optionTemplate'),
    routeName: '',
    $graphLinksContainer: null,

    // render elements
    $orderBy: null,
    $filterSearch: null,
    $numeric: null,
    $limit: null,

    initialize: function(options) {
      Yelp.Views.Filters.__super__.initialize.call(this);
      this.$content = $('#' + options.contentId);
      this.$graphLinksContainer = $('.graphLinks').find('a[href=#' + options.contentId + ']');
      this.routeName = options.routeName;
      this.bindEvents();
      _.bindAll(this, 'render', 'show', 'updateModels');
    },

    // bind view events
    bindEvents: function() {
      var self = this;
      this.$content.on('toggled', function() {
        self.show();
      });
      Yelp.Events.on('changed', function() {
        self.updateModels();
      });
    },

    // update models on url change
    updateModels: function() {
      this.resetModels();
      if (Yelp.Router.getCurrentRoute() === this.routeName) {
        var params = Yelp.Router.getCurrentQueryObject();
        var model = null;
        if (params) {
          for (var type in params) {
            switch (type) {
              case 'limit':
                model = this.collection.findByNameAndType('limit', 'limit');
                model.set('value', parseInt(params[type], 10));
                break;
              case 'orderBy':
                for (var orderName in params[type]) {
                  model = this.collection.findByNameAndType(orderName, 'orderBy');
                  if (model) {
                    model.set('value', params[type][orderName]);
                  }
                }
                break;
              case 'filter':
                for (var filterName in params[type]) {
                  if (typeof(params[type][filterName]) === 'object') {
                    // is numeric filter
                    model = this.collection.findByNameAndType(filterName, 'numeric');
                    if (model) {
                      for (var operator in params[type][filterName]) {
                        model.setNumeric(operator, params[type][filterName][operator]);
                      }
                    }
                  } else {
                    // is search filter
                    model = this.collection.findByNameAndType(filterName, 'search');
                    if (model) {
                      model.set('value', params[type][filterName]);
                    }
                  }
                }
                break;
            }
          }
          this.$graphLinksContainer.trigger('click');
        }
      }
    },

    show: function() {
      this.changed = false;
      this.render();
    },

    // render filter menu
    render: function() {
      var self = this;
      this.$content.empty();
      this.$el.empty();

      this.collection.each(function(model) {
        var $el;
        self['renderView' + model.get('type').capitalize()](model);
      }, this);

      // append rendered views
      this.appendLimit()
        .appendOrderBy()
        .appendSearch()
        .appendNumeric();

      // reset button
      this.buildResetButton();
      // search button
      this.buildSearchButton();

      if (this.changed) {
        this.$el.empty();
      } else {
        this.$content.append(this.$el.children());
      }
      return this;
    },

    // reset button and events
    buildResetButton: function() {
      var self = this;
      var $button = $('<div>')
        .addClass('reset-filters-button')
        .addClass('search-filter-label')
        .addClass('large-12')
        .addClass('columns')
        .html('Reset');

      self.$el.append($button);
      $button.on('click', function(event) {
        self.resetModels();
        self.show();
      });
    },

    // build search button and click event
    buildSearchButton: function() {
      var self = this;
      var $button = $('<div>')
        .addClass('search-button')
        .addClass('search-filter-label')
        .addClass('large-12')
        .addClass('columns')
        .addClass('active')
        .html('Search');

      self.$el.append($button);

      $button.on('click', function(event) {
        var filters = {};
        self.collection.each(function(model) {
          if (!model.isValid()) {
            console.log(model.validationError);
          } else {
            switch (model.get('type')) {
              case 'limit':
                filters.limit = model.getValue();
                break;

              case 'orderBy':
                if (model.getValue()) {
                  filters.orderBy = filters.orderBy || {};
                  filters.orderBy[model.get('name')] = model.getValue();
                }
                break;

              case 'search':
              case 'numeric':
                if (model.getValue()) {
                  filters.filter = filters.filter || {};
                  filters.filter[model.get('name')] = model.getValue();
                }
                break;
            }
          }
        }, self);
        Yelp.Router.setRoute(self.routeName, filters);
      });
    },

    // append a filter category to element and bind events
    appendFilterCategory: function(templateArgs, $el) {
      this.$el.prepend(this.template(templateArgs));

      this.$('.' + templateArgs.rootClass + ' > .search-filter-content').append($el.children());

      this.bindFilterEvents(
        this.$('.' + templateArgs.rootClass + ' > .search-filter-label'),
        this.$('.' + templateArgs.rootClass + ' > .search-filter-content > .back')
      );
    },

    // bind click and back events for each category
    bindFilterEvents: function($label, $back) {
      $label.on('click', function(event) {
        $label.closest('.search-filter').siblings().hide();
        $label.hide();
        $back.parent('.search-filter-content').addClass('visible');
      });

      $back.on('click', function(event) {
        $label.closest('.search-filter').siblings().show();
        $label.show();
        $back.parent('.search-filter-content').removeClass('visible');
      });
    },

    // build model view
    buildFilter: function(model) {
      var self = this;
      var viewName = model.get('type').capitalize() + 'Filter';
      var filterView = new Yelp.Views[viewName]({
        model: model
      });
      return filterView.render().$el.children();
    },

    // reset model values
    resetModels: function() {
      this.collection.each(function(model) {
        model.reset();
      });
    },

    // render each type of filter
    renderViewLimit: function(model) {
      this.$limit = this.$limit || $('<div></div>');
      this.$limit.append(this.buildFilter(model));
      return this;
    },

    appendLimit: function() {
      if (this.$limit) {
        this.$el.append(this.$limit.children());
      }
      this.$limit = null;
      return this;
    },

    renderViewOrderBy: function(model) {
      this.$orderBy = this.$orderBy || $('<div></div>');
      this.$orderBy.append(this.buildFilter(model));
      return this;
    },

    appendOrderBy: function() {
      // order by
      this.appendFilterCategory({
        name: 'Order',
        options: '',
        rootClass: 'order-by-parent',
      }, this.$orderBy);
      this.$orderBy = null;
      return this;
    },

    renderViewNumeric: function(model) {
      this.$numeric = this.$numeric || $('<div></div>');
      this.$numeric.append(this.buildFilter(model));
      return this;
    },

    appendNumeric: function() {
      // numeric filters
      this.appendFilterCategory({
        name: 'Filter',
        options: '',
        rootClass: 'filter-numeric-parent',
      }, this.$numeric);
      this.$numeric = null;
      return this;
    },

    renderViewSearch: function(model) {
      this.$filterSearch = this.$filterSearch || $('<div></div>');
      this.$filterSearch.append(this.buildFilter(model));
      return this;
    },

    appendSearch: function() {
      // search filters 
      this.appendFilterCategory({
        name: 'Search',
        options: '',
        rootClass: 'filter-search-parent',
      }, this.$filterSearch);
      this.filterSearch = null;
      return this;
    },

  });
}();
var Yelp = Yelp || {};
Yelp.Views = Yelp.Views || {};

! function() {

  "use strict";

  Yelp.Views.SearchFilter = Yelp.Views.BaseView.extend({
    model: Yelp.Models.Filter,
    tagName: 'div',
    // 
    template: Yelp.template('optionTemplate'),

    initialize: function() {
      Yelp.Views.SearchFilter.__super__.initialize.call(this);
      this.model.set('type', 'search');
      this.model.set('active', false);
    },

    render: function() {
      var self = this;
      var options = '' +
      '<input class="search-filter-input" type="text" value="' + this.model.get('value') + '"/>' + 
      '<div class="option remove-filter">Remove</div>' +
      '<div class="option add-filter">Add</div>';

      self.$el.html(self.template({
        'name': self.model.get('label'),
        'options': options,
      }));
      return self.bindEvents();
    },

    bindEvents: function() {
      var self = this;
      var $label = self.$('.search-filter-label');
      var $content = self.$('.search-filter-content');
      var $back = self.$('.search-filter-content .back');
      var $options = self.$('.search-filter-content .option');
      
      $label.on('click', function(event) {
        event.preventDefault();
        $label.closest('.search-filter').siblings().hide();
        $label.hide();
        $content.addClass('visible');
      });
      
      $back.on('click', function(event) {
        $label.closest('.search-filter').siblings().show();
        event.preventDefault();
        $label.show();
        $content.removeClass('visible');
        $content.find('.search-filter-input').val(self.model.get('value'));
      });

      $options.on('click', function(event) {
        event.preventDefault();
        var $el = $(this);
        if($el.hasClass('remove-filter'))
        {
          self.model.set('value', '');
          $content.find('.search-filter-input').val('');
        }
        else if ($el.hasClass('add-filter'))
        {
          var val = $content.find('.search-filter-input').val();
          self.model.set('value', val);
        }
        
        $back.trigger('click');
      });
      return self;
    },
  });
}();
var Yelp = Yelp || {};
Yelp.Views = Yelp.Views || {};

! function() {

  "use strict";

  Yelp.Views.NumericFilter = Yelp.Views.BaseView.extend({
    model: Yelp.Models.Filter,
    tagName: 'div',
    // 
    template: Yelp.template('optionTemplate'),
    numericTemplate: Yelp.template('numericFilterTemplate'),

    initialize: function() {
      Yelp.Views.NumericFilter.__super__.initialize.call(this);
      this.model.set('type', 'numeric');
      this.model.set('active', false);
    },

    render: function() {
      var self = this;
      var values = this.model.getNumericValues();
      var classes = {};
      for (var key in values) {
        if (values[key] !== '') {
          classes[key] = ' active';
        } else {
          classes[key] = '';
        }
      }

      var options = '' + this.numericTemplate({
          classes: classes,
          values: values,
          max: this.model.get('max', ''),
        }) +
        '<div class="option remove-filter">Remove</div>' +
        '<div class="option add-filter">Add</div>';

      self.$el.html(self.template({
        'name': self.model.get('label'),
        'options': options,
      }));
      return self.bindEvents();
    },

    bindEvents: function() {
      var self = this;
      var $label = self.$('.search-filter-label');
      var $content = self.$('.search-filter-content');
      var $back = self.$('.search-filter-content .back');
      var $optLabels = self.$('.search-filter-content .option > .numeric-filter-label');
      var $add = self.$('.search-filter-content .option.add-filter');
      var $remove = self.$('.search-filter-content .option.remove-filter');

      $label.on('click', function(event) {
        event.preventDefault();
        $label.closest('.search-filter').siblings().hide();
        $label.hide();
        $content.addClass('visible');
      });

      $back.on('click', function(event) {
        $label.closest('.search-filter').siblings().show();
        event.preventDefault();
        $label.show();
        $content.removeClass('visible');
        // 
        $optLabels.removeClass('active');
        $optLabels.siblings('input').val('');
        // 
        var values = self.model.getNumericValues();
        for (var filterName in values) {
          if (values[filterName] !== '') {
            $optLabels.filter('.' + filterName)
              .addClass('active')
              .siblings('input')
              .val(values[filterName]);
          }
        }
      });

      $optLabels.on('click', function(event) {
        var $this = $(this);
        var elClass = $this.attr('class').split(' ').join('.');
        if ($this.hasClass('eq')) {
          $optLabels.filter(':not(.' + elClass + ')')
            .siblings('input')
            .val('');
          $optLabels.removeClass('active');
        } else if ($this.hasClass('gt') || $this.hasClass('gte')) {
          $optLabels.filter(':not(.' + elClass + ')')
            .filter(':not(.lower)')
            .siblings('input')
            .val('');
          $optLabels.filter('.eq').removeClass('active');
          $optLabels.filter('.upper').removeClass('active');
        } else if ($this.hasClass('lt') || $this.hasClass('lte')) {
          $optLabels.filter(':not(.' + elClass + ')')
            .filter(':not(.upper)')
            .siblings('input')
            .val('');
          $optLabels.filter('.eq').removeClass('active');
          $optLabels.filter('.lower').removeClass('active');
        }
        $this.addClass('active');
      });

      $add.on('click', function(event) {
        var $activeFilters = $content.find('.option > .numeric-filter-label.active');
        if ($activeFilters.filter('.eq').length !== 0) {
          if ($activeFilters.length === 1) {
            var $input = $activeFilters.filter('.eq').siblings('input');
            if ($input.length !== 0 && $input.val().isNumeric()) {
              self.model.setNumeric('eq', parseInt($input.val(), 10));
            }
          }
        }

        if ($activeFilters.filter('.lower').length == 1) {
          var className = 'lt';
          if ($activeFilters.filter('.lower').hasClass('lte')) {
            className = 'lte';
          }
          var $input = $activeFilters.filter('.lower').siblings('input');
          if ($input.length !== 0 && $input.val().isNumeric()) {
            self.model.setNumeric(className, parseInt($input.val(), 10));
          }
        }

        if ($activeFilters.filter('.upper').length == 1) {
          var className = 'gt';
          if ($activeFilters.filter('.upper').hasClass('gte')) {
            className = 'gte';
          }
          var $input = $activeFilters.filter('.upper').siblings('input');
          if ($input.length !== 0 && $input.val().isNumeric()) {
            self.model.setNumeric(className, parseInt($input.val(), 10));
          }
        }
        $back.trigger('click');
      });

      $remove.on('click', function(event) {
        self.model.resetNumeric();
        $back.trigger('click');
      });
      return self;
    },
  });
}();
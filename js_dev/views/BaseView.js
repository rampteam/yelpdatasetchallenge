var Yelp = Yelp || {};
Yelp.Views = Yelp.Views || {};

! function() {

  "use strict";

  /**
   * Base class for all item views
   */
  Yelp.Views.BaseView = Backbone.View.extend({
    diameter: 640,
    $content: null,
    contentName: null,
    changed: false,
    initialize: function(options) {
      var self = this;
      this.$content = $('#content');
      this.contentName = '#content';
      this.diameter = this.$content.width();
      Yelp.Events.on('changed', function() {
        self.changed = true;
      });
      Backbone.View.prototype.initialize.call(options);
    },
  });
}();
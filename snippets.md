## Command
	
Upsert
 php artisan app:batch:upsert -f app/storage/data/dataset --env=packer-virtualbox


Create indexes
 php artisan app:batch:create:indexes --env=packer-virtualbox


API query format
 http://yelp.dev/api/ybusinesses?limit=10&filter[city]=Phoenix&columns[]=city&columns[]=name&search=Living
 

Text search indexes mongodb
You can configure text search in the mongo shell:
db.adminCommand( { setParameter : 1, textSearchEnabled : true } )

mongod --setParameter textSearchEnabled=true